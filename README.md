# Postgres utils

**Warning when using pg: ** Don't forget to call `client.end()` when fetching from pg pool in order to return it to the pool in a server program!

# Installation
`npm install` will throw errors if native postgres lib (`pg_config`) is not in the path. _dont fret, zs-pg will still work!_

# Migrations HOWTO

* `npm install -g db-migrate`
* `ZS_PG_*_PASSWORD` env vars

if you get error saying module pg isnt found, go to global db-migrate npm directory and npm install there. pg is listed as devDependency (bug).

## Migration sets

There are distinct migration sets under the migrations folder. To use one, supply
```--migrations-dir, -m        The directory containing your migration files.  [default: "./migrations"]```

## Creating

`db-migrate create <name> --sql-file -e test -m migrations/<set dir>`

and edit the created sql files

## Running

`db-migrate up/down -e <pg lane> -m migrations/<set dir>`

# Usage:


# Users:
* scripts
* realestate-service
* wmt
* pipeline
