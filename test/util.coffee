assert = require 'assert'
util = require '../lib/util'

describe.only 'util', ->
  it 'should fill an SQL query template with supplied parameter values', () ->
    filled = util.fillQuery "SELECT * FROM foo WHERE $1 = $1 AND array_length($2, 1) > 0 AND $3 = 'bar' AND $4 AND NULL IS $5 AND $10 = 'has not been replaced with $1'", [ 15, ['a','b'], "Tom's", false, null, 6, 7, 8, 9, 'TEST' ]
    assert.equal filled, "SELECT * FROM foo WHERE 15 = 15 AND array_length(ARRAY['a', 'b'], 1) > 0 AND 'Tom\\'s' = 'bar' AND FALSE AND NULL IS NULL AND 'TEST' = 'has not been replaced with 15'"

  it "createUpdateQuery", ->
    console.log util.createUpdateQuery({string:"string", number: 5, array: [1,2,3], id: 234}, "table", "id").sqlFilled