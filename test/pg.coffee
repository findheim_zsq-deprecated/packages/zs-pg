assert = require 'assert'

pg = require("../lib/pg")

describe 'pg', () ->
  it 'should return a client', ->
    client = pg.getClient()
    assert(client?)
  it 'should be able to select stuff', (done) ->
    client = pg.getClient()
    client.connect (err) ->
      if err?
        done err
      client.query "SELECT 1 as success", (err,res)->
        if err?
          done err
        assert(res.rows[0]["success"] is 1)
        done()

  it 'getClientFromPool()', (done) ->
    pg.getClientFromPool (err, client) ->
      if err?
        done err
      client.query "SELECT 1 as success", (err,res)->
        if err?
          done err
        assert(res.rows[0]["success"] is 1)
        done()

  it "queryPool(sql, cb)", (done) ->
    pg.queryPool "SELECT 1 as success", (err,res) ->
      if err?
        done err
      else
        assert(res.rows[0]["success"] is 1)
        done()

  it "queryPool(sql, cb, existinglane) succeeds", (done) ->
    pg.queryPool "SELECT 1 as success", (err,res) ->
      if err?
        done err
      else
        assert(res.rows[0]["success"] is 1)
        done()
    , "at.index"

  it "queryPool(sql, cb, nonexistinglane) fails", (done) ->
    pg.queryPool "SELECT 1 as fail", (err,res) ->
      if err?
        done()
      else
        done new Error "Could select from notexistinglane"
    , "notexistinglane"

  it "queryPool(sql, params, cb)", (done) ->
    pg.queryPool "SELECT $1::numeric as success", [2], (err,res) ->
      if err?
        done err
      else
        assert res.rows[0]["success"] - 2 is 0 # returns string?
        done()

  it "queryPool(sql, params, cb, existinglane)", (done) ->
    pg.queryPool "SELECT $1::numeric as success", [2], (err,res) ->
      if err?
        done err
      else
        assert res.rows[0]["success"] - 2 is 0 # returns string?
        done()
    , "at.index"

  it "queryPool(sql, params, cb, nonexistinglane) fails", (done) ->
    pg.queryPool "SELECT $1 as fail", [2], (err,res) ->
      if err?
        done()
      else
        done new Error "Could select from notexistinglane"
    , "notexistinglane"
