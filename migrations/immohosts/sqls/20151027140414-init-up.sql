CREATE TABLE immohost
(
  fqdn text NOT NULL,
  created timestamp with time zone DEFAULT now(),
  sitemap_urls text[],
  openimmo_urls text[],
  crawl_delay int,
  owners text[],
  state text default 'awaiting_profile',
  comment text,
  issues text,
  estimated_ad_count int,
  last_estate_found timestamp with time zone,
  last_offline_found timestamp with time zone,
  last_zoomlive_push timestamp with time zone,
  crawler_id text default 'crawler1.zoomsquare.com',

  testpipeline boolean default false,
  prioritize_new_estates boolean default false,
  justimmo boolean,
  push_disabled boolean,
  prefer_https boolean default false,

  check_selectors text[],
  image_selector text,
  image_attr text default 'src',
  id_regex text,
  follow_regex text,
  title_selector text,

  fragments jsonb,
  seeds jsonb,
  zionic jsonb,

  rights_big_images boolean default false,
  rights_gallery boolean default false,

  CONSTRAINT "immohost_fqdn_primary_key" PRIMARY KEY ("fqdn")
);

CREATE TABLE immohost_changes (
  fqdn text references immohost(fqdn),
  time timestamp with time zone default now(),
  diff jsonb,
  comment text
);

CREATE index immohost_fqdn_idx on immohost using btree (fqdn);
CREATE index immohost_changes_fqdn_idx on immohost_changes using btree (fqdn);
CREATE index immohost_changes_time_idx on immohost_changes using btree (time);