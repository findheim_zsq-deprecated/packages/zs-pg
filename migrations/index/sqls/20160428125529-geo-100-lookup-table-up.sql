
create extension if not exists postgres_fdw;
DROP SERVER if exists bdm2 cascade;

CREATE SERVER bdm2
  FOREIGN DATA WRAPPER postgres_fdw
  OPTIONS (host 'bdm2.zoomsquare.com', port '5432',dbname 'bdm');
CREATE USER MAPPING
  FOR zoomsquare
  SERVER bdm2
  OPTIONS (user 'zoomsquare', password 'BbJepNkGKnhXgvi0D6HTRbsDW4GrvX');
CREATE FOREIGN TABLE loc_entity
   (id character varying(64) NOT NULL,
    country_code character varying(2) NOT NULL,
    nogeocoding boolean NOT NULL,
    types integer[] NOT NULL,
    blacklisted boolean NOT NULL,
    parents character varying(64)[],
    master character varying(64),
    geo geometry(Geometry,4326) )
   SERVER bdm2
   OPTIONS (schema_name 'bigdatamaps', table_name 'loc_entity');

CREATE FOREIGN TABLE loc_entity_geo_fgn
   (id character varying(64) NOT NULL,
    geo_100 geometry(Geometry,4326),
    nbefore integer,
    nafter integer)
   SERVER bdm2
   OPTIONS (schema_name 'bigdatamaps', table_name 'loc_entity_geo');

CREATE TABLE loc_entity_geo
(
  id character varying(64),
  geo_100 geometry(Geometry,4326) not null,
  nbefore integer,
  nafter integer,
  CONSTRAINT loc_entity_geo_id_pkey PRIMARY KEY (id)
);

CREATE INDEX loc_entity_geo_id_idx
  ON loc_entity_geo
  USING btree
  (id);

CREATE OR REPLACE FUNCTION bdm_simplify_to_n_points(
    geo geometry,
    npoints integer)
  RETURNS geometry AS
$BODY$
DECLARE
    orig_srid integer;
    orig_npoints integer;
    new_geo geometry;
    new_npoints integer;
    param float;
BEGIN
    orig_npoints = ST_NPoints(geo);

    if orig_npoints <= npoints then
      return bdm_prepareGeoJSON(geo);
    end if;

    orig_srid = ST_SRID(geo);

    IF orig_srid != 900913 THEN
        geo = ST_Transform(geo, 900913);
    END IF;

    param = 0.01*sqrt(ST_Area(geo));
    new_npoints = orig_npoints;
    new_geo = geo;
    while (new_npoints > npoints) loop
      new_geo = ST_SimplifyPreserveTopology(geo, param);
      if new_npoints = ST_NPoints(new_geo) then
        exit;
      end if;
      new_npoints = ST_NPoints(new_geo);
      --raise notice 'param: % -> % -> %', param, orig_npoints, new_npoints;
      if new_npoints < 150 then
        param = 1.5*param;
      else
        param = 2*param;
      end if;

    end loop;

    IF orig_srid != 900913 THEN
        new_geo = ST_Transform(new_geo, orig_srid);
    END IF;

    -- we got alot of invalid repeated points after conversion to geoJSON
    -- we try to fix this here
    RETURN bdm_prepareGeoJSON(new_geo);
END;
$BODY$
  LANGUAGE plpgsql IMMUTABLE STRICT
  COST 100;
ALTER FUNCTION bdm_simplify_to_n_points(geometry, integer)
  OWNER TO zoomsquare;

CREATE OR REPLACE FUNCTION bdm_prepareGeoJSON(geo geometry)
  RETURNS geometry AS
$BODY$
BEGIN
    -- we got alot of invalid repeated points after conversion to geoJSON
    -- we try to fix this here
    RETURN ST_makeValid(ST_RemoveRepeatedPoints(ST_geomFromGeoJSON(ST_asGeoJSON(geo,6))));
END;
$BODY$
  LANGUAGE plpgsql IMMUTABLE STRICT
  COST 100;
ALTER FUNCTION bdm_prepareGeoJSON(geometry)
  OWNER TO zoomsquare;

-- run this on bdm
CREATE OR REPLACE FUNCTION zs_populate_loc_entity_geo()
returns void as
$BODY$
BEGIN
    truncate loc_entity_geo;
    INSERT INTO loc_entity_geo
    SELECT id, bdm_simplify_to_n_points(geo, 100), st_npoints(geo), st_npoints(bdm_simplify_to_n_points(geo, 100))
    FROM loc_entity_geo_fgn;

END;
$BODY$
  LANGUAGE plpgsql
  COST 100;

-- run this on index servers
CREATE OR REPLACE FUNCTION zs_populate_loc_entity_geo_from_fgn()
returns void as
$BODY$
BEGIN

    truncate loc_entity_geo;
    INSERT INTO loc_entity_geo
    SELECT id, ST_SetSRID(geo_100,4326), nbefore, nafter
    FROM loc_entity_geo_fgn;

END;
$BODY$
  LANGUAGE plpgsql
  COST 100;