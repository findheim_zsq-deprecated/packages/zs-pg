-- SELECT cast(zs_score_v1(70::numeric, 500::numeric, 3::numeric, 12::numeric, COALESCE(0::numeric, NULL::numeric, NULL::numeric), COALESCE( 1000::numeric, NULL::numeric, NULL::numeric ), ARRAY[]::text[], ARRAY[]::integer[], (extract(epoch from now())::integer - zs_unixtime(p.created)), p ) as numeric(6,3)) from real_estates p

CREATE OR REPLACE FUNCTION zs_score_v1(
    minspace numeric,
    maxspace numeric,
    minrooms numeric,
    maxrooms numeric,
    minprice numeric,
    maxprice numeric,
    features text[],
    featureimportances integer[],
    timedifference integer,
    p real_estates)
  RETURNS numeric AS
$BODY$
  import math
  # import re
  import json
  scores = []

  if minspace is None:
    minspace_float = None
  else:
    minspace_float = float(minspace)

  if maxspace is None:
    maxspace_float = None
  else:
    maxspace_float = float(maxspace)

  if minrooms is None:
    minrooms_float = None
  else:
    minrooms_float = float(minrooms)

  if maxrooms is None:
    maxrooms_float = None
  else:
    maxrooms_float = float(maxrooms)

  if minprice is None:
    minprice_float = None
  else:
    minprice_float = float(minprice)

  if maxprice is None:
    maxprice_float = None
  else:
    maxprice_float = float(maxprice)

  ### Scoring functions ###
  # calculates f(val) where f is a band function defined by:
  #     val ranges: hardmin <= min          <=  max         <= hardmax
  #     f(val)      0          hardminval       hardmaxval     0
  # params:
  #     ramp_diff: desired slope between min and max
  #     strength:
  def band_custom_ramp(val, hardmin, hardminval, min, max, hardmax, hardmaxval, strength, ramp_diff, weight):

      if min is None and max is None:
          return

      if min is None:
          min = 0.0
      if max is None:
          max = 2.0**32
      if hardmin is None:
          hardmin = min
      if hardmax is None:
          hardmax = max

      if val is None:
          res = 0.0
      elif min <= val <= max: # inside user entered bounds
          if max == min:
              res = 1.0
          else:
              if ramp_diff < 0:
                  tomax = 1.0*(val - min)/(max-min)
              else:
                  tomax = 1.0*(max - val)/(max-min)
              res = 1.0 - tomax*math.fabs(ramp_diff)
      elif val <= hardmin or val >= hardmax: # outside hardmax bounds
          res = 0.0
      else: # outside user entered bounds, inside hardmax bounds
          if val > max:
              tohard = 1.0 * (val - max) / (hardmax - max)
              hardval = hardmaxval
          else:
              tohard = 1.0 * (val - min) / (hardmin - min)
              hardval = hardminval

          res = 1.0 * hardval / (1 + strength * math.exp(tohard * 12 - 6))

      scores.append([res, weight])

  #  def diffScore(prop, lookingFor, weight):
  #    if lookingFor == None or lookingFor == 0:
  #      return
  #    res = None
  #    val = p.get(prop)
  #    if val != None:
  #      if val == lookingFor:
  #        res = 1
  #      else:
  #        difference = abs(((val/lookingFor) - 1) * 100)
  #        if difference < 1:
  #          res = 1
  #        else:
  #          res = 1.0 / difference
  #    else:
  #      res = 0.1
  #    scores.append([res, weight])

  #  def midBandLow(prop, min, max, strength, weight):
  #    if min == None or min == 0 or max == None or max == 0:
  #      return
  #    val = p.get(prop)
  #    if val == None:
  #      return
  #    res = None
  #    if val != None:
  #      if val >= min and val <= max:
  #        res = 1
  #      elif val > max:
  #        res = 0.001
  #      elif val < min:
  #        res = 1.0 - (abs((val/min) - 1) * (strength * math.sqrt(val)))
  #      if res < 0:
  #        res = 0
  #    else:
  #      res = 0.01
  #    scores.append([res, weight])
  #
  #  def midBandHigh(prop, min, max, strength, weight):
  #    if min == None or min == 0 or max == None or max == 0:
  #      return
  #    val = p.get(prop)
  #    if val == None:
  #      return
  #    res = None
  #    if val != None:
  #      if val >= min and val <= max:
  #        res = 1
  #      elif val > max:
  #        res = 1.0 - (abs((val/max) - 1) * (strength * math.sqrt(val)))
  #      elif val < min:
  #        res = 0.001
  #      if res < 0:
  #        res = 0
  #    else:
  #      res = 0.01
  #    scores.append([res, weight])

  #  def boolScore(prop, lookingFor, weight):
  #    if lookingFor == None:
  #      return
  #    res = None
  #    val = p.get(prop)
  #    if val == None:
  #      res = 0.1
  #    else:
  #      if val == lookingFor:
  #        res = 1
  #      else:
  #        res = 0
  #    scores.append([res, weight])

  #  def typeScore(weight):
  #    for ptype in p.get("type"):
  #      res = 1.0
  #      for qtype in types:
  #        if ptype == qtype:
  #          scores.append([res, weight])
  #          return
  #        res = res / 2

  # def geoScore(weight):
  #   if geocontained:
  #     scores.append([1, weight])
  #   else:
  #     scores.append([0.01, weight])

  def ageScore(differenceInSeconds, weight):
    if differenceInSeconds - 60 * 60 * 24 * 7 > 0:
      scores.append([0.0001, weight])
    else:
      ageScore = 1 - (differenceInSeconds / (60 * 60 * 24 * 7 * 1.0))
      scores.append([ageScore, weight])

  #  def penalize(weight):
  #    scores.append([0.0001, weight])
  #
  #  def boni(weight):
  #    scores.append([1.0, weight])

  ageScore(timedifference, 12.0)

  props = json.loads(p.get("props"))

  if props.get("rentalFee") is not None:
    band_custom_ramp(props.get("rentalFee"),     0.5 * minprice_float, 0.95, minprice_float, maxprice_float, 1.1 * maxprice_float, 0.9, 1.0, -0.1, 17.0)
  elif props.get("purchasePrice") is not None:
    band_custom_ramp(props.get("purchasePrice"), 0.5 * minprice_float, 0.95, minprice_float, maxprice_float, 1.1 * maxprice_float, 0.9, 1.0, -0.1, 17.0)
  elif props.get("anualLeaseFee") is not None:
    band_custom_ramp(props.get("anualLeaseFee"), 0.5 * minprice_float, 0.95, minprice_float, maxprice_float, 1.1 * maxprice_float, 0.9, 1.0, -0.1, 17.0)

  if minrooms_float is not None and maxrooms_float is not None:
    band_custom_ramp(props.get("rooms"), minrooms_float, 1.0, minrooms_float, maxrooms_float, 2.0 * maxrooms_float, 0.95, 1.0, 0.1, 15.0)
  if minspace_float is not None and maxspace_float is not None:
    band_custom_ramp(props.get("livingSpace"), 0.85 * minspace_float, 0.9, minspace_float, maxspace_float, 2.0 * maxspace_float, 0.95, 1.0, 0.1,15.0)

  # We don't have plotarea, but asked for it, so punish the Estate
  # if p.get("plotarea") == None and plotarea != None:
  #   penalize(2.0)

  # penalize big sites w/o large images, TODO: remove this hard coded hack
  #  if re.search('(immobilien\.net|immoversum\.com|finden\.at|derstandard\.at)', p.get('url', '')):
  #    return 1

  if p.get("bigImages") is None or len(p.get("bigImages")) is 0:
      scores.append([0.0,10.0])
  else:
      scores.append([1.0,10.0])

  # FEATURES
  # filter out boolean props (features) and save their key names
  estate_features = dict(filter(lambda (key,val): type(val) is bool, props.iteritems())).keys()
  if estate_features is None:
      estate_features = []

  # fmax is the maximum attainable importance sum
  fmax = 0.0

  for imp in featureimportances:
    fmax += imp

  if fmax > 0: # we have wanted features
      fscore = 0.0

      for i, wantedfeature in enumerate(features):
          if estate_features.count(wantedfeature) > 0:  # estate has wanted feature
              fscore += featureimportances[i]

      scores.append([fscore / fmax, 18.0])
  else: # no wanted features: just add number of estate features
      scores.append([min(len(estate_features),20.0)/20.0, 18 ])

  # Weightening
  dividend = 0.0
  divisor = 0.0
  for score in scores:
    dividend = dividend + (score[0] * score[1])
    divisor = divisor + score[1]

  if divisor == 0:
    return 0.0
  else:
    return dividend / divisor * 100.0
$BODY$
  LANGUAGE plpython2u VOLATILE
  COST 100;
ALTER FUNCTION zs_score_v1(numeric, numeric, numeric, numeric, numeric, numeric, text[], integer[], integer, real_estates)
  OWNER TO zoomsquare;
