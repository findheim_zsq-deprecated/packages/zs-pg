/* Replace with your SQL commands */



CREATE OR REPLACE FUNCTION zs_estate_ads_constraint()
  RETURNS trigger AS
$BODY$

DECLARE ads text[];
BEGIN
    ads := array(select a."extId" from real_estate_ads a join real_estates r on a."real_estates_extId" = r."extId" where r."extId"=new."extId");

    IF new.dups @> ads and ads @> new.dups  and array_length(new.dups,1) = array_length(ads,1) THEN
        RETURN new;
    ELSE
        RAISE EXCEPTION 'Estate % dups constraint violation', new."extId";
    END IF;

END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION zs_estate_ads_constraint()
  OWNER TO zoomsquare;


CREATE CONSTRAINT TRIGGER zs_estate_ads_constraint_trigger
  AFTER INSERT OR UPDATE
  ON real_estates
  INITIALLY DEFERRED
  FOR EACH ROW
  EXECUTE PROCEDURE zs_estate_ads_constraint();


ALTER table real_estate_ads
ALTER "real_estates_extId" SET NOT NULL;
