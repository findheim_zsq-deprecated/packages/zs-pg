/* Replace with your SQL commands */

drop materialized view if exists mapping_andy;
drop materialized view if exists mapping_from_mongo_zionic;
drop view if exists mapping_geo;
drop view if exists mapping_georesult;
drop view if exists mapping_georesult3;
drop view if exists mapping_intersected;
drop view if exists mapping_searchprofiles;

drop table if exists intersected_lookup;
drop table if exists mapping;
drop table if exists mapping_georesult3_ids;
drop table if exists mapping_georesult_ids;
drop table if exists mapping_mongo;
drop table if exists mapping_zionic;
drop table if exists mappingtable;


DROP FUNCTION if exists zs_lookup_intersected(real_estates);
DROP FUNCTION if exists zs_lookup_intersected_single(text);
DROP FUNCTION if exists zs_topo3_calculate_georesult3(json, json);
DROP FUNCTION if exists zs_topo3_calculate_ids(text[]);
DROP FUNCTION if exists zs_topo3_calculate_intersected(text, integer, jsonb);
DROP FUNCTION if exists zs_topo3_calculate_mapping(text);
DROP FUNCTION if exists zs_topo3_calculate_mapping_usetopo(text);
DROP FUNCTION if exists zs_topo3_migrate(text, integer);
DROP FUNCTION if exists zs_topo3_migrate_georesult(text, integer);
DROP FUNCTION if exists zs_topo3_migrate_intersected(text);
DROP FUNCTION if exists zs_deleteme_topotype_string(jsonb);
DROP FUNCTION if exists zs_deleteme_topo(jsonb);
DROP FUNCTION if exists zs_calculate_intersected_single(text, integer);
DROP FUNCTION if exists zs_calculate_intersected();

alter table real_estates drop column if exists intersected2;
alter table real_estates drop column if exists intersected3;
alter table real_estates drop column if exists "geoResult2";
alter table real_estates drop column if exists "geoResult3";

alter table real_estate_ads drop column if exists district2;
alter table real_estate_ads drop column if exists "geo2";
alter table real_estate_ads drop column if exists "geo3";


alter table real_estates alter column "geoResult" type jsonb using "geoResult"::text::jsonb;


CREATE OR REPLACE FUNCTION zs_jsonb_arr2text_arr(_js jsonb)
  RETURNS text[] AS
$BODY$
SELECT ARRAY(SELECT jsonb_array_elements_text(_js))
$BODY$
  LANGUAGE sql IMMUTABLE
  COST 100;

CREATE OR REPLACE FUNCTION zs_upd_real_estates()
  RETURNS trigger AS
$BODY$
  BEGIN

    IF (TG_OP = 'INSERT' OR (OLD.props::text IS DISTINCT FROM NEW.props::text)) THEN
       NEW = zs_calculate_columns(NEW);
    END IF;

    IF ((TG_OP = 'INSERT') OR
      (OLD."geoResult"->>'intersected') <> (NEW."geoResult"->>'intersected') OR (OLD."geoResult"->>'fake') = 'true') or new.intersected is null THEN

      new.intersected = zs_jsonb_arr2text_arr((NEW."geoResult"->'intersected')::jsonb);

    END IF;

    NEW.modified = NOW();
    RETURN NEW;
  END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;


DROP FUNCTION if exists zs_upsert_newpipe(text, text, text, text[], jsonb, json, json, text, text[], boolean, timestamp with time zone, timestamp with time zone, text[], text[], boolean[], jsonb);

CREATE OR REPLACE FUNCTION zs_upsert_newpipe(
    url_val text,
    extid_val text,
    fqdn_val text,
    dups_val text[],
    props_val jsonb,
    georesult_val jsonb,
    sources_val json,
    thumb_val text,
    bigimages_val text[],
    online_val boolean,
    created_val timestamp with time zone,
    updated_val timestamp with time zone,
    dups_fqdns_val text[],
    dups_urls_val text[],
    dups_online_val boolean[],
    stats_val jsonb)
  RETURNS void AS
$BODY$
BEGIN
  LOOP
    UPDATE
      real_estates
    SET
      url = url_val,
      "extId" = extId_val,
      fqdn = fqdn_val,
      dups = dups_val,

      props = props_val,
      "geoResult" = geoResult_val,
      sources = sources_val,

      thumb = thumb_val,
      "bigImages" = bigImages_val,

      online = online_val,
      created = created_val,
      updated = updated_val,

      dups_fqdns = dups_fqdns_val,
      dups_urls = dups_urls_val,
      dups_online = dups_online_val,
      stats = stats_val
    WHERE
      "extId" = extId_val;
    IF found THEN
      EXIT;
    END IF;
    BEGIN
      INSERT INTO
        real_estates(
          url,
          "extId",
          fqdn,
          dups,

          props,
          "geoResult",
          sources,

          thumb,
          "bigImages",

          online,
          created,
          updated,

          dups_fqdns,
          dups_urls,
          dups_online,
          stats
        ) VALUES (
          url_val,
          extId_val,
          fqdn_val,
          dups_val,

          props_val,
          geoResult_val,
          sources_val,

          thumb_val,
          bigImages_val,

          online_val,
          created_val,
          updated_val,

          dups_fqdns_val,
          dups_urls_val,
          dups_online_val,
          stats_val
        );
      EXIT;
    EXCEPTION WHEN unique_violation THEN
      -- LOOP AGAIN!
    END;
  END LOOP;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
