CREATE OR REPLACE FUNCTION zs_array_intersect(
    array1 text[],
    array2 text[])
  RETURNS text[] AS
$BODY$
declare
    res text[];
begin
    if array1 is null then
        return array2;
    elseif array2 is null then
        return array1;
    end if;
    select array_agg(e) into res
    from (
        select unnest(array1)
        intersect
        select unnest(array2)
    ) as dt(e);
    return res;
end;
$BODY$
  LANGUAGE plpgsql IMMUTABLE
  COST 100;
ALTER FUNCTION zs_array_intersect(text[], text[])
  OWNER TO zoomsquare;

alter function zs_calculate_columns(real_estates) immutable;
alter function zs_calculate_intersected(real_estates) immutable;
alter function zs_calculate_intersected_single(json, boolean) immutable;
ALTER FUNCTION zs_score_v1(numeric, numeric, numeric, numeric, numeric, numeric, text[], integer[], integer, real_estates) immutable;
ALTER FUNCTION zs_score_v1(numeric, numeric, numeric, numeric, numeric, numeric, text[], integer[], text[], integer[], integer, real_estates) immutable;

--ALTER FUNCTION zs_score_v1(double precision, double precision, double precision, double precision, double precision, double precision, text[], integer[], integer, real_estates) immutable;