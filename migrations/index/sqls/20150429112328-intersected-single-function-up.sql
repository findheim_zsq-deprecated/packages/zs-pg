
-- more feedback on intersected errors

CREATE OR REPLACE FUNCTION zs_calculate_intersected_single(
    geoResult json,
    online boolean)
  RETURNS text[] AS
$BODY$
DECLARE r text[];
BEGIN
  IF NOT online OR cast(geoResult->>'type' as int) < 6 OR geoResult IS NULL THEN
      r = ARRAY['filtered_by_calculate_intersected']::text[]; -- clear and ignore if real estate not geocoded at least at rural district level (6)
  ELSE

        SELECT array_agg(l.id) into r
        FROM loc_entity l, loc_entity p
        WHERE
            (geoResult->>'id') = p.id
            AND NOT l.blacklisted
            AND l.master IS NULL -- exclude slaves (which have a master ref)
            AND l.types && ARRAY[2,4,6,8,9,10,12] -- only those which can be selected by search queries
            AND l.types[1] <= p.types[1]
            AND l.geo_orig && p.geo_orig
            AND ST_Relate(
				-- use tolerance buffer of 40m because of unaligned edges
				(CASE WHEN GeometryType(p.geo_orig) IN ('LINESTRING', 'MULTILINESTRING') THEN
	    			-- take parent of street
			    	ST_Transform(ST_Buffer(ST_Transform((SELECT pp.geo_orig FROM loc_entity pp WHERE p.parents[1] = pp.id), 900913), -40), 4326)
        		WHEN GeometryType(p.geo_orig) IN ('MULTIPOLYGON', 'POLYGON') THEN
                    ST_Transform(ST_Buffer(ST_Transform(p.geo_orig, 900913), -40), 4326)
                ELSE
                    p.geo_orig -- POINT
                END), l.geo_orig, 'T********'); -- only overlapping, no touching; with tolerance factor, so that we don't get neighbors into intersected
    IF r is null THEN
      r = ARRAY['no_intersection_found']::text[];
    END IF;
  END IF;

  RETURN r;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION zs_calculate_intersected_single(json, boolean)
  OWNER TO zoomsquare;

-- use _single function here for easier consistency

CREATE OR REPLACE FUNCTION zs_calculate_intersected(r real_estates)
RETURNS real_estates AS
$BODY$
BEGIN
    r.intersected = zs_calculate_intersected_single(r."geoResult", r.online);
    return r;
END;
$BODY$
LANGUAGE plpgsql VOLATILE
COST 100;
ALTER FUNCTION zs_calculate_intersected(real_estates)
OWNER TO zoomsquare;


-- the conditions dont work, and produce empty arrays on update

CREATE OR REPLACE FUNCTION zs_upd_real_estates()
  RETURNS trigger AS
$BODY$
  BEGIN

    --IF (TG_OP = 'INSERT' or (OLD.props::text IS DISTINCT FROM NEW.props::text)) THEN
        NEW = zs_calculate_columns(NEW);
    --END IF;

    --IF (TG_OP = 'INSERT' or (OLD."geoResult"::text IS DISTINCT FROM NEW."geoResult"::text)) THEN
        NEW.intersected = zs_calculate_intersected_single(NEW."geoResult",NEW.online);
    --END IF;

    NEW.modified = NOW();
    RETURN NEW;
  END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION zs_upd_real_estates()
  OWNER TO zoomsquare;
