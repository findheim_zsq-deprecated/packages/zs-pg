  CREATE OR REPLACE FUNCTION final_median(anyarray) RETURNS float8 AS
  $$
  DECLARE
    cnt INTEGER;
  BEGIN
    cnt := (SELECT count(*) FROM unnest($1) val WHERE val IS NOT NULL);
    RETURN (SELECT avg(tmp.val)::float8
              FROM (SELECT val FROM unnest($1) val
                      WHERE val IS NOT NULL
                      ORDER BY 1
                      LIMIT 2 - MOD(cnt, 2)
                      OFFSET CEIL(cnt/ 2.0) - 1
                    ) AS tmp
           );
  END
  $$ LANGUAGE plpgsql;

  CREATE AGGREGATE median(anyelement) (
    SFUNC=array_append,
    STYPE=anyarray,
    FINALFUNC=final_median,
    INITCOND='{}'
  );
