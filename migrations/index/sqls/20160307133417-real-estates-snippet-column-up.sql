alter table real_estates
  add column seo_snippets text[];

CREATE OR REPLACE FUNCTION zs_upsert_newpipe2(
    url_val text,
    extid_val text,
    fqdn_val text,
    dups_val text[],
    props_val jsonb,
    georesult_val jsonb,
    sources_val json,
    thumb_val text,
    bigimages_val text[],
    online_val boolean,
    created_val timestamp with time zone,
    updated_val timestamp with time zone,
    dups_fqdns_val text[],
    dups_urls_val text[],
    dups_online_val boolean[],
    stats_val jsonb,
    seo_snippets_val text[])
  RETURNS void AS
$BODY$
BEGIN
  LOOP
    UPDATE
      real_estates
    SET
      url = url_val,
      "extId" = extId_val,
      fqdn = fqdn_val,
      dups = dups_val,

      props = props_val,
      "geoResult" = geoResult_val,
      sources = sources_val,

      thumb = thumb_val,
      "bigImages" = bigImages_val,

      online = online_val,
      created = created_val,
      updated = updated_val,

      dups_fqdns = dups_fqdns_val,
      dups_urls = dups_urls_val,
      dups_online = dups_online_val,
      stats = stats_val,
      seo_snippets = seo_snippets_val
    WHERE
      "extId" = extId_val;
    IF found THEN
      EXIT;
    END IF;
    BEGIN
      INSERT INTO
        real_estates(
          url,
          "extId",
          fqdn,
          dups,

          props,
          "geoResult",
          sources,

          thumb,
          "bigImages",

          online,
          created,
          updated,

          dups_fqdns,
          dups_urls,
          dups_online,
          stats,
          seo_snippets
        ) VALUES (
          url_val,
          extId_val,
          fqdn_val,
          dups_val,

          props_val,
          geoResult_val,
          sources_val,

          thumb_val,
          bigImages_val,

          online_val,
          created_val,
          updated_val,

          dups_fqdns_val,
          dups_urls_val,
          dups_online_val,
          stats_val,
          seo_snippets_val
        );
      EXIT;
    EXCEPTION WHEN unique_violation THEN
      -- LOOP AGAIN!
    END;
  END LOOP;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION zs_upsert_newpipe2(text, text, text, text[], jsonb, jsonb, json, text, text[], boolean, timestamp with time zone, timestamp with time zone, text[], text[], boolean[], jsonb, text[])
  OWNER TO zoomsquare;