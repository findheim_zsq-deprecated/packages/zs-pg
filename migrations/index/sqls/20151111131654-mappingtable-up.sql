/* Replace with your SQL commands */

create table mappingtable (
    topo2_id text PRIMARY KEY,
    topo2_types int[],
    topo2_parents text[],
    topo2_community text,
    topo2_rural_district text,
    topo2_state text,
    topo2_label text,
    topo2_name text,
    match text,
    topo3_id text,
    topo3_types int[],
    topo3_parents text[],
    topo3_community text,
    topo3_rural_district text,
    topo3_state text,
    topo3_label text,
    topo3_name text,
    topo3_synames text[],
    topo3_city boolean,
    topo3_master boolean,
    topo3_topo jsonb
);


--topo2_id, topo2_types, topo2_parents, topo2_community, topo2_rural_district, topo2_state, topo2_label, topo2_name, match, topo3_id, topo3_types, topo3_parents, topo3_community, topo3_rural_district, topo3_state, topo3_label, topo3_name, topo3_synames, topo3_city, topo3_master, topo3_topo
--topo2_id;topo2_types;topo2_parents;topo2_community;topo2_rural_district;topo2_state;topo2_label;topo2_name;match;topo3_id;topo3_types;topo3_parents;topo3_community;topo3_rural_district;topo3_state;topo3_label;topo3_name;topo3_synames;topo3_city;topo3_master;topo3_topo