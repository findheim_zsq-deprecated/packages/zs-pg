CREATE OR REPLACE FUNCTION zs_push_notify(data text, profileid text)
  RETURNS void AS
$BODY$
BEGIN
  PERFORM pg_notify('push', data);
  UPDATE searchprofiles SET "lastPush" = NOW() WHERE id = profileid;
  --RAISE Warning 'Push notification triggered for search profile % with data %', profileid, data;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION zs_push_notify(text, text)
  OWNER TO zoomsquare;

-----------------------------------------------

CREATE OR REPLACE FUNCTION zs_upsert_searchprofile(
    rentalfee_val numeric[],
    purchaseprice_val numeric[],
    anualleasefee_val numeric[],
    livingspace_val numeric[],
    rooms_val numeric[],
    geoentities_val text[],
    type_val text,
    id_val text,
    featurekeys_val text[],
    featurevals_val integer[],
    enabled_val boolean,
    nocommission_val boolean
  ) RETURNS VOID AS
$$
BEGIN
  LOOP
    UPDATE
      searchprofiles
    SET
      "rentalFee" = rentalfee_val,
      "purchasePrice" = purchaseprice_val,
      "annualLeaseFee" = anualleasefee_val,
      "livingSpace" = livingspace_val,
      rooms = rooms_val,
      "geoEntities" = geoentities_val,
      type = type_val,
      id = id_val,
      featurekeys = featurekeys_val,
      featurevals = featurevals_val,
      enabled = enabled_val,
      "noCommission" = nocommission_val
    WHERE
      id = id_val;
    IF found THEN
      EXIT;
    END IF;
    BEGIN
      INSERT INTO searchprofiles(
        "rentalFee",
        "purchasePrice",
        "annualLeaseFee",
        "livingSpace",
        rooms,
        "geoEntities",
        type,
        id,
        featurekeys,
        featurevals,
        enabled,
        "noCommission"
      ) VALUES (
        rentalfee_val,
        purchaseprice_val,
        anualleasefee_val,
        livingspace_val,
        rooms_val,
        geoentities_val,
        type_val,
        id_val,
        featurekeys_val,
        featurevals_val,
        enabled_val,
        nocommission_val
      );
    EXIT;
    EXCEPTION WHEN unique_violation THEN
      -- LOOP AGAIN!
    END;
  END LOOP;
END;
$$
LANGUAGE plpgsql;

ALTER FUNCTION zs_upsert_searchprofile(numeric[],numeric[],numeric[],numeric[],numeric[],text[],text,text,text[],integer[],boolean,boolean)
  OWNER TO zoomsquare;

-----------------------------------------------

CREATE OR REPLACE FUNCTION zs_reverse_query()
  RETURNS trigger AS
$BODY$
BEGIN
  IF array_length(new.dups, 1) = 1 THEN
    PERFORM
      zs_push_notify(row_to_json(row)::text, row.id::text)
    FROM (
      SELECT
        sp.id,
        new."extId",
        new.type,
        new."geoResult"->>'label' as address,
        new.props->>'livingSpace' as "livingSpace",
        COALESCE(new.props->>'rentalFee', new.props->>'purchasePrice', new.props->>'annualLeaseFee') as price,
        (
          CASE
            WHEN (new.props->>'rentalFee') IS NOT NULL THEN 'rent'
            WHEN (new.props->>'purchasePrice') IS NOT NULL THEN 'purchase'
            WHEN (new.props->>'annualLeaseFee') IS NOT NULL THEN 'lease'
          END
        ) as "ownType"
      FROM searchprofiles sp
      WHERE
        (sp."lastPush" IS NULL OR sp."lastPush" < (now() - interval '3 hours')) -- push only every 3 hours
        AND sp.enabled -- push only to push enabled
        AND sp."geoEntities" && new.intersected -- geo must match
        AND sp.type = ANY(new.type) -- type must match
        AND ( --
          CASE
            WHEN (sp."rentalFee")      IS NOT NULL THEN (new.props->>'rentalFee')::numeric      BETWEEN sp."rentalFee"[1] AND sp."rentalFee"[2]
            WHEN (sp."purchasePrice")  IS NOT NULL THEN (new.props->>'purchasePrice')::numeric  BETWEEN sp."purchasePrice"[1] AND sp."purchasePrice"[2]
            WHEN (sp."annualLeaseFee") IS NOT NULL THEN (new.props->>'annualLeaseFee')::numeric BETWEEN sp."annualLeaseFee"[1] AND sp."annualLeaseFee"[2]
          END
        )
        AND (
          CASE
            WHEN (sp."livingSpace") IS NOT NULL THEN
              (new.props->>'livingSpace') IS NOT NULL AND (
                ((new.props->>'livingSpace')::numeric BETWEEN sp."livingSpace"[1] AND sp."livingSpace"[2]) OR
                (sp."livingSpace"[1] IS NULL AND sp."livingSpace"[2] >= (new.props->>'livingSpace')::numeric) OR
                (sp."livingSpace"[2] IS NULL AND sp."livingSpace"[1] <= (new.props->>'livingSpace')::numeric)
              )
            ELSE
              TRUE
          END
        )
        AND (
          CASE
            WHEN (sp.rooms) IS NOT NULL THEN
              (new.props->>'rooms') IS NOT NULL AND (
                ((new.props->>'rooms')::numeric BETWEEN sp.rooms[1] AND sp.rooms[2]) OR
                (rooms[1] IS NULL AND rooms[2] >= (new.props->>'rooms')::numeric) OR
                (rooms[2] IS NULL AND rooms[1] <= (new.props->>'rooms')::numeric)
              )
          ELSE
            TRUE
          END
        )
        AND (
          CASE WHEN sp."noCommission"::boolean THEN
            (new.props->>'noCommission') IS NOT NULL AND (new.props->>'noCommission')::boolean
          ELSE
            TRUE
          END
        )
      ) row;
  END IF;
  RETURN new;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION zs_reverse_query()
  OWNER TO zoomsquare;

-----------------------------------------------

CREATE TRIGGER zs_push_trigger
  AFTER INSERT
  ON real_estates
  FOR EACH ROW
  EXECUTE PROCEDURE zs_reverse_query();