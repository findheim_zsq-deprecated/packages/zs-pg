CREATE TABLE searchprofiles
(
  "rentalFee" numeric(15,2)[],
  "purchasePrice" numeric(15,2)[],
  "annualLeaseFee" numeric(15,2)[],
  "livingSpace" numeric(15,2)[],
  rooms numeric(15,2)[],
  "geoEntities" text[],
  type text,
  id text NOT NULL,
  featurekeys text[],
  featurevals integer[],
  enabled boolean,
  "noCommission" boolean,
  "lastPush" timestamp without time zone,
  CONSTRAINT searchprofiles_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE searchprofiles
  OWNER TO zoomsquare;

-- Index: enabled

-- DROP INDEX enabled;

CREATE INDEX enabled
  ON searchprofiles
  USING btree
  (enabled);

-- Index: searchprofiles_anualleasefee_idx

-- DROP INDEX searchprofiles_anualleasefee_idx;

CREATE INDEX searchprofiles_anualleasefee_idx
  ON searchprofiles
  USING btree
  ("annualLeaseFee");

-- Index: searchprofiles_geoentities_idx

-- DROP INDEX searchprofiles_geoentities_idx;

CREATE INDEX searchprofiles_geoentities_idx
  ON searchprofiles
  USING gin
  ("geoEntities" COLLATE pg_catalog."default");

-- Index: searchprofiles_livingspace_idx

-- DROP INDEX searchprofiles_livingspace_idx;

CREATE INDEX searchprofiles_livingspace_idx
  ON searchprofiles
  USING btree
  ("livingSpace");

-- Index: searchprofiles_nocommission_idx

-- DROP INDEX searchprofiles_nocommission_idx;

CREATE INDEX searchprofiles_nocommission_idx
  ON searchprofiles
  USING btree
  ("noCommission");

-- Index: searchprofiles_purchaseprice_idx

-- DROP INDEX searchprofiles_purchaseprice_idx;

CREATE INDEX searchprofiles_purchaseprice_idx
  ON searchprofiles
  USING btree
  ("purchasePrice");

-- Index: searchprofiles_rentalfee_idx

-- DROP INDEX searchprofiles_rentalfee_idx;

CREATE INDEX searchprofiles_rentalfee_idx
  ON searchprofiles
  USING btree
  ("rentalFee");

-- Index: searchprofiles_rooms_idx

-- DROP INDEX searchprofiles_rooms_idx;

CREATE INDEX searchprofiles_rooms_idx
  ON searchprofiles
  USING btree
  (rooms);

-- Index: searchprofiles_type_idx

-- DROP INDEX searchprofiles_type_idx;

CREATE INDEX searchprofiles_type_idx
  ON searchprofiles
  USING hash
  (type COLLATE pg_catalog."default");