/* Replace with your SQL commands */

alter function zs_array_intersect(text[],text[]) volatile;
alter function zs_calculate_columns(real_estates) volatile;
alter function zs_calculate_intersected(real_estates) volatile;
alter function zs_calculate_intersected_single(json, boolean) volatile;
ALTER FUNCTION zs_score_v1(numeric, numeric, numeric, numeric, numeric, numeric, text[], integer[], integer, real_estates) volatile;
ALTER FUNCTION zs_score_v1(numeric, numeric, numeric, numeric, numeric, numeric, text[], integer[], text[], integer[], integer, real_estates) volatile;
--ALTER FUNCTION zs_score_v1(double precision, double precision, double precision, double precision, double precision, double precision, text[], integer[], integer, real_estates) volatile;

DROP FUNCTION zs_array_intersect(text[], text[]);