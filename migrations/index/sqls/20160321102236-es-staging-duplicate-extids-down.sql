CREATE OR REPLACE FUNCTION zs_real_estates_staging_trigger_function()
  RETURNS trigger AS
$BODY$
  BEGIN

    if (TG_OP = 'DELETE') then
        delete from real_estates_staging where "staging_extId" = OLD."extId";
        insert into real_estates_staging ("staging_extId", "staging_op", "staging_time") VALUES (OLD."extId", TG_OP, OLD.modified);
    else
        delete from real_estates_staging where "staging_extId" = NEW."extId";
        insert into real_estates_staging ("staging_extId", "staging_op", "staging_time") VALUES (NEW."extId", TG_OP, NEW.modified);
    end if;

    RETURN null;
  END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

drop table real_estates_staging;
create table real_estates_staging (
    staging_id bigserial not null unique,
    "staging_extId" text PRIMARY KEY not null,
    "staging_op" text not null,
    "staging_time" timestamp with time zone DEFAULT now() not null
);

CREATE INDEX "real_estates_staging_staging_extId_idx" ON real_estates_staging USING btree ("staging_extId");
CREATE INDEX "real_estates_staging_staging_id_idx" ON real_estates_staging USING btree (staging_id);
CREATE INDEX "real_estates_staging_staging_op_idx" ON real_estates_staging USING btree (staging_op);