/* Replace with your SQL commands */

CREATE TABLE real_estate_ads
(
    "extId" text NOT NULL,
    "district" text,
    "props" jsonb,
    "geo" jsonb,
    "fragments" jsonb,
    "online" boolean NOT NULL,
    "fqdn" text NOT NULL,
    "url" text NOT NULL,
    "overwritten" boolean,
    "created" timestamp with time zone,
    "lastCrawled" timestamp with time zone,
    "lastPipelined" timestamp with time zone,
    "lastOffline" timestamp with time zone,
    "real_estates_extId" text,
    CONSTRAINT "extId_ads" PRIMARY KEY ("extId"),
    CONSTRAINT "real_estates_ads_real_estates_extId_fkey" FOREIGN KEY ("real_estates_extId")
        REFERENCES real_estates ("extId") MATCH SIMPLE
        ON UPDATE NO ACTION ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED
);

DROP INDEX IF EXISTS real_estates_id_idx;

ALTER TABLE real_estates
    DROP COLUMN IF EXISTS id;