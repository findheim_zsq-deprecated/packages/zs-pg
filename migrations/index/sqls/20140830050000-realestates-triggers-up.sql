
CREATE OR REPLACE FUNCTION zs_calculate_columns(r real_estates)
  RETURNS real_estates AS
$BODY$
  BEGIN
    r."livingSpace" = cast(r.props->>'livingSpace' as numeric);
    r."plotArea" = cast(r.props->>'plotArea' as numeric);
    r."usableSpace" = cast(r.props->>'usableSpace' as numeric);
    r."roomSpace" = cast(r.props->>'roomSpace' as numeric);

    r."rentalFee" = cast(r.props->>'rentalFee' as numeric);
    r."purchasePrice" = cast(r.props->>'purchasePrice' as numeric);
    r."annualLeaseFee" = cast(r.props->>'annualLeaseFee' as numeric);

    r.rooms = cast(r.props->>'rooms' as numeric);
    if (r."props"->>'type') IS NULL or (r."props"->>'type') = '' or (r."props"->>'type') = '[]' then
        r.type = ARRAY[]::text[]; -- fix for saveToPostgresNewFormat: Could not push to Postgres: cannot call json_array_elements on a scalar: (type was null)
    else
        r.type = (SELECT array_agg(trim(el::text, '"')) FROM json_array_elements(r."props"->'type') el)::text[];
    END IF;

    IF (r."geoResult"->>'precision') != 'georef' THEN
        r.geo = (SELECT ST_SetSRID(ST_GeomFromGeoJSON(r."geoResult"->>'geo'), 4326));
    END IF;

    RETURN r;
  END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;


CREATE OR REPLACE FUNCTION zs_calculate_intersected(r real_estates)
  RETURNS real_estates AS
$BODY$
BEGIN
  IF NOT r.online OR cast(r."geoResult"->>'type' as int) < 6 OR r."geoResult" IS NULL THEN
      r.intersected = ARRAY[]::text[]; -- clear and ignore if real estate not geocoded at least at rural district level (6)
  ELSE
        SELECT array_agg(l.id) INTO r.intersected
        FROM loc_entity l, loc_entity p
        WHERE
            (r."geoResult"->>'id') = p.id
            AND NOT l.blacklisted
            AND l.master IS NULL -- exclude slaves (which have a master ref)
            AND l.types && ARRAY[2,4,6,8,9,10,12] -- only those which can be selected by search queries
            AND l.types[1] <= p.types[1]
            AND l.geo_orig && p.geo_orig
            AND ST_Relate(
				-- use tolerance buffer of 40m because of unaligned edges
				(CASE WHEN GeometryType(p.geo_orig) IN ('LINESTRING', 'MULTILINESTRING') THEN
	    			-- take parent of street
			    	ST_Transform(ST_Buffer(ST_Transform((SELECT pp.geo_orig FROM loc_entity pp WHERE p.parents[1] = pp.id), 900913), -40), 4326)
        		WHEN GeometryType(p.geo_orig) IN ('MULTIPOLYGON', 'POLYGON') THEN
                    ST_Transform(ST_Buffer(ST_Transform(p.geo_orig, 900913), -40), 4326)
                ELSE
                    p.geo_orig -- POINT
                END), l.geo_orig, 'T********'); -- only overlapping, no touching; with tolerance factor, so that we don't get neighbors into intersected
  END IF;
  RETURN r;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION zs_calculate_intersected(real_estates)
  OWNER TO zoomsquare;


-- update trigger calls other functions
CREATE OR REPLACE FUNCTION zs_upd_real_estates()
  RETURNS trigger AS
$BODY$
  BEGIN

    IF (TG_OP = 'INSERT' or (OLD.props::text IS DISTINCT FROM NEW.props::text)) THEN
        NEW = zs_calculate_columns(NEW);
    END IF;

    IF (TG_OP = 'INSERT' or (OLD."geoResult"::text IS DISTINCT FROM NEW."geoResult"::text)) THEN
        NEW = zs_calculate_intersected(NEW);
    END IF;

    NEW.modified = NOW();
    RETURN NEW;
  END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

CREATE TRIGGER zs_upd_real_estates_trigger
  BEFORE INSERT OR UPDATE
  ON real_estates
  FOR EACH ROW
  EXECUTE PROCEDURE zs_upd_real_estates();

CREATE OR REPLACE FUNCTION zs_newproperty_notify()
  RETURNS trigger AS
$BODY$BEGIN
    IF array_length(NEW.dups, 1) > 0 THEN
        PERFORM pg_notify('newproperty', row_to_json(row)::text) FROM (
            SELECT
            -- similar format than zs_reverse_query
            -- but no searchprofile id
            -- +url
            -- ownType instead of reasoning via prices... => and domain is "rented, purchased, leased" NOT "rent", etc.!
            -- +viewType
            -- +all spaces
            -- +lat, lng for map display
                NEW."extId",
                NEW.url,
                NEW.type,
                NEW.props->>'viewType' AS "viewType",
                NEW.props->>'ownType' AS "ownType",
                NEW."geoResult"->>'label' AS address,
                NEW.props->>'livingSpace' AS "livingSpace",
                NEW.props->>'plotArea' AS "plotArea",
                NEW.props->>'roomSpace' AS "roomSpace",
                NEW.props->>'usableSpace' AS "usableSpace",
                COALESCE(NEW.props->>'rentalFee', NEW.props->>'purchasePrice', NEW.props->>'annualLeaseFee') AS price,
                NEW.props->>'rooms' AS rooms,
                NEW.props->>'noCommission' AS "noCommission",
                NEW.props->>'investmentOnly' AS "investmentOnly",
                NEW.props->>'cooperative' AS cooperative,
                NEW.props->>'openSpace' AS "openSpace",
                NEW.props->>'balcony' AS balcony,
                NEW.props->>'terrace' AS terrace,
                NEW.props->>'roofTerrace' AS "roofTerrace",
                NEW.props->>'pool' AS pool,
                NEW.props->>'wintergarden' AS wintergarden,
                NEW.props->>'roofTop' AS "roofTop",
                NEW.props->>'nearbyUnderground' AS "nearbyUnderground",
                NEW.props->>'needsRenovation' AS "needsRenovation",
                NEW.props->>'barrierFree' AS "barrierFree",
                NEW.props->>'wheelChairSuitable' AS "wheelChairSuitable",
                NEW.props->>'newBuilding' AS "newBuilding",
                NEW.props->>'oldBuilding' AS "oldBuilding",
                NEW."geoResult"->>'lat' AS lat,
                NEW."geoResult"->>'lng' AS lng,
                NEW."geoResult"->>'precision' AS precision,
                NEW.intersected,
                NEW.props->'units' AS units,
                NEW.thumb,
                NEW.created
            ) row;
    END IF;
    RETURN new;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION zs_newproperty_notify()
  OWNER TO zoomsquare;


CREATE TRIGGER zs_newproperty_trigger
  AFTER INSERT
  ON real_estates
  FOR EACH ROW
  EXECUTE PROCEDURE zs_newproperty_notify();

