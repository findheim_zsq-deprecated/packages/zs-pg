/* Replace with your SQL commands */

create table intersected_lookup (
    id text PRIMARY KEY,
    intersected text[]
);

create index id_idx on intersected_lookup using hash(id);

DROP FUNCTION zs_calculate_intersected_single(json, boolean);
DROP FUNCTION zs_calculate_intersected(real_estates);

-- zs_lookup_intersected_single
CREATE OR REPLACE FUNCTION zs_lookup_intersected_single(in_id text)
  RETURNS text[] AS
$BODY$
DECLARE
  r text[];
BEGIN
  IF in_id IS NULL THEN
       r = ARRAY['filtered_by_calculate_intersected','georesult_null']::text[];
  ELSE
    SELECT intersected::text[] FROM intersected_lookup WHERE id = in_id INTO r;
  END IF;
  RETURN r;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

-- zs_lookup_intersected
CREATE OR REPLACE FUNCTION zs_lookup_intersected(r real_estates)
  RETURNS real_estates AS
$BODY$
BEGIN
    r.intersected = zs_lookup_intersected_single(r."geoResult"->>'id');
    return r;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;


CREATE OR REPLACE FUNCTION zs_calculate_intersected()
  RETURNS void AS
$BODY$
DECLARE
  r text[];
  parent text;
BEGIN
  truncate intersected_lookup;
  insert into intersected_lookup (select id, zs_calculate_intersected_single(id,(select max(x) from unnest(types) x)) as intersected from loc_entity);
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;


CREATE OR REPLACE FUNCTION zs_calculate_intersected_single(in_id text,in_type int)
  RETURNS text[] AS
$BODY$
DECLARE
  r text[];
  parent text;
BEGIN
  IF in_type < 6 THEN
       r = ARRAY['filtered_by_calculate_intersected','type<6']::text[]; -- clear and ignore if real estate not geocoded at least at rural district level (6)
  ELSE
    SELECT parents::text[] FROM loc_entity WHERE id = in_id INTO r;
    IF in_type <= 10 THEN
      r = r || ARRAY[in_id]; -- add itself if selectable for search (type <= 10)
    END IF;
  END IF;
  RETURN r;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

CREATE OR REPLACE FUNCTION zs_upd_real_estates()
  RETURNS trigger AS
$BODY$
  BEGIN

    IF (TG_OP = 'INSERT' OR
        (OLD.props::text IS DISTINCT FROM NEW.props::text) OR
        (OLD."geoResult"::text IS DISTINCT FROM NEW."geoResult"::text)) THEN
            NEW = zs_calculate_columns(NEW);
            NEW.intersected = zs_lookup_intersected_single(NEW."geoResult"->>'id');
    END IF;

    NEW.modified = NOW();
    RETURN NEW;
  END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

--select zs_calculate_intersected();