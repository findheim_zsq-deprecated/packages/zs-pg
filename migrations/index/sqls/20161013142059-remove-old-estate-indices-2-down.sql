/* Replace with your SQL commands */

CREATE INDEX "real_estates_uniqueEstates_idx"
  ON real_estates
  USING btree
  ((fqdn = ALL (dups_fqdns)));
