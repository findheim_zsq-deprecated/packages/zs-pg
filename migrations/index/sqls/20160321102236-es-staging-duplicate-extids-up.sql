CREATE OR REPLACE FUNCTION zs_real_estates_staging_trigger_function()
  RETURNS trigger AS
$BODY$
  DECLARE
    changed_geo boolean = false;
  BEGIN

    if (TG_OP = 'DELETE') then
        insert into real_estates_staging ("staging_extId", "staging_op", "staging_time") VALUES (OLD."extId", TG_OP, OLD.modified);
    elsif (TG_OP = 'INSERT') then
        insert into real_estates_staging ("staging_extId", "staging_op", "staging_time", "staging_changed_props", "staging_changed_geoResult", "staging_changed_geo")
          VALUES (NEW."extId", TG_OP, NEW.modified, true, true, true);
    else
      changed_geo = ((OLD."geoResult"->>'precision') <> (NEW."geoResult"->>'precision'))
        OR ((OLD."geoResult"->>'precision') = 'georef' AND (OLD."geoResult"->>'id') <> (NEW."geoResult"->>'id'))
        OR ((OLD."geoResult"->>'precision') = 'approx' AND (OLD."geoResult"->>'geo') is distinct from (NEW."geoResult"->>'geo'))
        OR ((OLD."geoResult"->>'precision') = 'point' AND (OLD."geoResult"->>'geo') is distinct from (NEW."geoResult"->>'geo'));

      insert into real_estates_staging ("staging_extId", "staging_op", "staging_time", "staging_changed_props", "staging_changed_geoResult", "staging_changed_geo")
        VALUES (NEW."extId", TG_OP, NEW.modified, OLD.props::text is distinct from NEW.props::text, OLD."geoResult"::text is distinct from NEW."geoResult"::text, changed_geo);
    end if;

    RETURN null;
  END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

drop table real_estates_staging;
create table real_estates_staging (
    staging_id bigserial PRIMARY KEY,
    "staging_extId" text not null,
    "staging_op" text not null,
    "staging_time" timestamp with time zone DEFAULT now() not null,
    "staging_changed_props" boolean,
    "staging_changed_geoResult" boolean,
    "staging_changed_geo" boolean
);

CREATE INDEX "real_estates_staging_staging_extId_idx" ON real_estates_staging USING btree ("staging_extId");
CREATE INDEX "real_estates_staging_staging_id_idx" ON real_estates_staging USING btree (staging_id);
CREATE INDEX "real_estates_staging_staging_op_idx" ON real_estates_staging USING btree (staging_op);