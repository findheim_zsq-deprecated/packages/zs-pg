-- Function: zs_calculate_intersected_single(json, boolean)

-- DROP FUNCTION zs_calculate_intersected_single(json, boolean);

CREATE OR REPLACE FUNCTION zs_calculate_intersected_single(
    georesult json,
    online boolean)
  RETURNS text[] AS
$BODY$
DECLARE
  r text[];
  parent text;
BEGIN
  IF NOT online OR cast(geoResult->>'type' as int) < 6 OR geoResult IS NULL THEN
      r = ARRAY['filtered_by_calculate_intersected']::text[]; -- clear and ignore if real estate not geocoded at least at rural district level (6)
  ELSE
    SELECT parents::text[] FROM loc_entity WHERE id = geoResult->>'id' INTO r;
    IF (geoResult->>'type')::int <= 10 THEN
      r = r || ARRAY[geoResult->>'id']; -- add itself if selectable for search (type <= 10)
    END IF;
  END IF;
  RETURN r;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION zs_calculate_intersected_single(json, boolean)
  OWNER TO zoomsquare;