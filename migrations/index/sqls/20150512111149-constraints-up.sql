/* Replace with your SQL commands */

ALTER TABLE real_estates ALTER COLUMN created SET NOT NULL;
ALTER TABLE real_estates ADD CONSTRAINT estates_created_not_too_old CHECK (created > '2013-01-01');
ALTER TABLE real_estates ALTER COLUMN online SET NOT NULL;


ALTER TABLE real_estate_ads ALTER COLUMN online SET NOT NULL;
ALTER TABLE real_estate_ads ALTER COLUMN created SET NOT NULL;
ALTER TABLE real_estate_ads ALTER COLUMN "lastCrawled" SET NOT NULL;
ALTER TABLE real_estate_ads ALTER COLUMN "lastWrittenToPG" SET NOT NULL;
ALTER TABLE real_estate_ads ALTER COLUMN "lastPipelined" SET NOT NULL;
ALTER TABLE real_estate_ads ADD CONSTRAINT ads_created_not_too_old CHECK (created > '2013-01-01');
ALTER TABLE real_estate_ads ADD CONSTRAINT last_offline_not_null CHECK (online or "lastOffline" is not null);






