CREATE EXTENSION IF NOT EXISTS plpython2u;
CREATE EXTENSION IF NOT EXISTS intarray;

-- postgis -> schema postgis
CREATE SCHEMA IF NOT EXISTS postgis;
CREATE EXTENSION IF NOT EXISTS postgis WITH SCHEMA postgis;

ALTER DATABASE zoomsquare
  SET search_path = public, bigdatamaps, postgis;

-- zs_unixtime --
CREATE OR REPLACE FUNCTION zs_unixtime(input_time timestamp with time zone)
  RETURNS integer AS
$BODY$
  BEGIN
    RETURN extract(epoch from input_time)::integer;
  END;
$BODY$
  LANGUAGE plpgsql IMMUTABLE STRICT
  COST 100;

-- table real_estate
-- search index for fast searching with detailed data as json props, geo, images, sources, etc.
-- numeric fields in separate columns for faster indexing (did some tests, it was faster ;-)
-- we use text instead of varchar: http://www.depesz.com/2010/03/02/charx-vs-varcharx-vs-varchar-vs-text/
CREATE TABLE real_estates
(
-- basic info
  id text NOT NULL,
  "extId" text NOT NULL,
  fqdn text,
  url text,
  thumb text,
-- dups info
  dups text[],
  dups_fqdns text[],
  dups_urls text[],
  dups_online boolean[],

-- all props, images, geo, and sources as JSON
  props json,
  "geoResult" json,
  sources json,

-- calculated by triggers: externalize numeric props, arrays, and geo
  "livingSpace" numeric(10,2),
  "plotArea" numeric(10,2),
  "usableSpace" numeric(10,2),
  "roomSpace" numeric(10,2),
  "rentalFee" numeric(15,2),
  "purchasePrice" numeric(15,2),
  "annualLeaseFee" numeric(15,2),
  rooms numeric(5,1),
  type text[],
  geo geometry,

-- derived/calculated by triggers from other fields
  intersected text[],

-- meta data
  "bigImages" text[],
  online boolean,
  created timestamp with time zone,
  modified timestamp with time zone,
  updated timestamp with time zone,
  CONSTRAINT "extId" PRIMARY KEY ("extId")
);

CREATE INDEX "real_estates_id_idx" ON real_estates USING hash (id);
CREATE INDEX "real_estates_extId_idx" ON real_estates USING hash ("extId");