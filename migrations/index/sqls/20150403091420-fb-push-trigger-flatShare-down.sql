CREATE OR REPLACE FUNCTION zs_newproperty_notify()
  RETURNS trigger AS
$BODY$
BEGIN
    IF (new.created IS NOT NULL AND (now() - new.created) < (interval '5 minutes')) THEN
        PERFORM pg_notify('newproperty', row_to_json(row)::text) FROM (
            SELECT
            -- similar format than zs_reverse_query
            -- but no searchprofile id
            -- +url
            -- ownType instead of reasoning via prices... => and domain is "rented, purchased, leased" NOT "rent", etc.!
            -- +viewType
            -- +all spaces
            -- +lat, lng for map display
                NEW."extId",
                NEW.url,
                NEW.type,
                NEW.props->>'viewType' AS "viewType",
                NEW.props->>'ownType' AS "ownType",
                NEW."geoResult"->>'label' AS address,
                NEW.props->>'livingSpace' AS "livingSpace",
                NEW.props->>'plotArea' AS "plotArea",
                NEW.props->>'roomSpace' AS "roomSpace",
                NEW.props->>'usableSpace' AS "usableSpace",
                COALESCE(NEW.props->>'rentalFee', NEW.props->>'purchasePrice', NEW.props->>'annualLeaseFee') AS price,
                NEW.props->>'rooms' AS rooms,
                NEW.props->>'noCommission' AS "noCommission",
                NEW.props->>'investmentOnly' AS "investmentOnly",
                NEW.props->>'cooperative' AS cooperative,
                NEW.props->>'openSpace' AS "openSpace",
                NEW.props->>'balcony' AS balcony,
                NEW.props->>'terrace' AS terrace,
                NEW.props->>'roofTerrace' AS "roofTerrace",
                NEW.props->>'pool' AS pool,
                NEW.props->>'wintergarden' AS wintergarden,
                NEW.props->>'roofTop' AS "roofTop",
                NEW.props->>'nearbyUnderground' AS "nearbyUnderground",
                NEW.props->>'needsRenovation' AS "needsRenovation",
                NEW.props->>'barrierFree' AS "barrierFree",
                NEW.props->>'wheelChairSuitable' AS "wheelChairSuitable",
                NEW.props->>'newBuilding' AS "newBuilding",
                NEW.props->>'oldBuilding' AS "oldBuilding",
                NEW."geoResult"->>'lat' AS lat,
                NEW."geoResult"->>'lng' AS lng,
                NEW."geoResult"->>'precision' AS precision,
                NEW.intersected,
                NEW.props->'units' AS units,
                NEW.thumb,
                NEW.created
            ) row;
    END IF;
    RETURN new;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION zs_newproperty_notify()
  OWNER TO zoomsquare;