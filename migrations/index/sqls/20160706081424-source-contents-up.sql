CREATE TABLE ad_sources(
    "extId" text PRIMARY KEY,
    content text NOT NULL,
    source  text NOT NULL
);

CREATE OR REPLACE FUNCTION zs_upsert_ad_sources(
    extId_val   text,
    content_val text,
    source_val  text
) RETURNS void AS
$BODY$
BEGIN
  LOOP
    UPDATE
      ad_sources
    SET
      "content" = content_val,
      "source" = source_val
    WHERE
      "extId" = extId_val;
    IF found THEN
      EXIT;
    END IF;
    BEGIN
      INSERT INTO ad_sources(
        "extId",
        source,
        content
      ) VALUES (
        extId_val,
        source_val,
        content_val
      );
    EXIT;
    EXCEPTION WHEN unique_violation THEN
      -- LOOP AGAIN!
    END;
  END LOOP;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE;