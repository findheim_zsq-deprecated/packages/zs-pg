CREATE INDEX real_estate_ads_oi_ftppath_idx
  ON real_estate_ads
  USING btree
  ((oi ->> 'ftppath'::text) COLLATE pg_catalog."default")
  WHERE oi IS NOT NULL;