/* Replace with your SQL commands */

DROP INDEX if EXISTS "real_estates_props_noCommission_null_idx";
DROP INDEX IF EXISTS "real_estates_props_noCommission_idx";
DROP INDEX IF EXISTS "real_estates_props__ver_idx";
DROP INDEX IF EXISTS "real_estates_created_idx";
DROP INDEX IF EXISTS "real_estates_bigImagesNull_idx";
DROP INDEX IF EXISTS "real_estates_annualLeaseFee_idx";
