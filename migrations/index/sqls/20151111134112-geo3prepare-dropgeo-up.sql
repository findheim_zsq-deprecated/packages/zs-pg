CREATE OR REPLACE FUNCTION zs_calculate_columns(r real_estates)
  RETURNS real_estates AS
$BODY$
  BEGIN
    r."livingSpace" = cast(r.props->>'livingSpace' as numeric);
    r."plotArea" = cast(r.props->>'plotArea' as numeric);
    r."usableSpace" = cast(r.props->>'usableSpace' as numeric);
    r."roomSpace" = cast(r.props->>'roomSpace' as numeric);

    r."rentalFee" = cast(r.props->>'rentalFee' as numeric);
    r."purchasePrice" = cast(r.props->>'purchasePrice' as numeric);
    r."annualLeaseFee" = cast(r.props->>'annualLeaseFee' as numeric);

    r.rooms = cast(r.props->>'rooms' as numeric);
    if (r."props"->>'type') IS NULL or (r."props"->>'type') = '' or (r."props"->>'type') = '[]' then
        r.type = ARRAY[]::text[]; -- fix for saveToPostgresNewFormat: Could not push to Postgres: cannot call json_array_elements on a scalar: (type was null)
    else
        r.type = (SELECT array_agg(trim(el::text, '"')) FROM jsonb_array_elements(r."props"->'type') el)::text[];
    END IF;

    RETURN r;
  END;
$BODY$
  LANGUAGE plpgsql IMMUTABLE
  COST 100;
ALTER FUNCTION zs_calculate_columns(real_estates)
  OWNER TO zoomsquare;

alter table real_estates drop column geo;