CREATE INDEX "immohosts_fqdn_idx" ON immohosts using btree (fqdn);
CREATE INDEX "immohosts_blacklist_days_idx" ON immohosts using gin (blacklist_days);
CREATE INDEX "immohosts_blacklist_days_created_idx" ON immohosts using btree (blacklist_days_created);
CREATE INDEX "immohosts_created_idx" ON immohosts using btree (created);


CREATE OR REPLACE FUNCTION zs_timeonmarket(a real_estate_ads)
    RETURNS interval AS
$BODY$
DECLARE
    blacklist date[];
BEGIN
    select blacklist_days into blacklist from immohosts where fqdn=a.fqdn;
    if a.created::date = ANY(blacklist) then
       return null;
    else
        if a.online then
            RETURN now() - a.created;
        else
            RETURN a."lastOffline" - a.created;
	    end if;
    end if;
END;
$BODY$
    LANGUAGE plpgsql VOLATILE
    COST 100;

CREATE OR REPLACE FUNCTION zs_timeonmarket(real_estates)
  RETURNS interval AS
$$
select zs_timeonmarket(a) from real_estate_ads a where a."real_estates_extId" = $1."extId" order by created limit 1;
$$
  LANGUAGE sql VOLATILE
  COST 100;

CREATE OR REPLACE FUNCTION zs_timeonmarket(text)
  RETURNS interval AS
$$
select zs_timeonmarket(a) from real_estate_ads a where a."real_estates_extId" = $1 order by created limit 1;
$$
  LANGUAGE sql VOLATILE
  COST 100;