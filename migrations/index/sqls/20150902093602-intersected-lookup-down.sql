drop function zs_calculate_intersected_single(text,int);
drop function zs_calculate_intersected();
drop function zs_lookup_intersected(r real_estates);
drop function zs_lookup_intersected_single(text);
drop table intersected_lookup cascade;

CREATE OR REPLACE FUNCTION zs_calculate_intersected(r real_estates)
  RETURNS real_estates AS
$BODY$
BEGIN
    r.intersected = zs_calculate_intersected_single(r."geoResult", r.online);
    return r;
END;
$BODY$
  LANGUAGE plpgsql IMMUTABLE
  COST 100;
ALTER FUNCTION zs_calculate_intersected(real_estates)
  OWNER TO zoomsquare;

CREATE OR REPLACE FUNCTION zs_calculate_intersected_single(
    georesult json,
    online boolean)
  RETURNS text[] AS
$BODY$
DECLARE
  r text[];
  parent text;
BEGIN
  IF NOT online THEN
       r = ARRAY['filtered_by_calculate_intersected','not_online']::text[]; -- clear and ignore if real estate not geocoded at least at rural district level (6)
  ELSIF cast(geoResult->>'type' as int) < 6 THEN
       r = ARRAY['filtered_by_calculate_intersected','type<6']::text[]; -- clear and ignore if real estate not geocoded at least at rural district level (6)
  ELSIF geoResult IS NULL THEN
       r = ARRAY['filtered_by_calculate_intersected','georesult_null']::text[]; -- clear and ignore if real estate not geocoded at least at rural district level (6)
  ELSE
    SELECT parents::text[] FROM loc_entity WHERE id = geoResult->>'id' INTO r;
    IF (geoResult->>'type')::int <= 10 THEN
      r = r || ARRAY[geoResult->>'id']; -- add itself if selectable for search (type <= 10)
    END IF;
  END IF;
  RETURN r;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION zs_calculate_intersected_single(json, boolean)
  OWNER TO zoomsquare;

CREATE OR REPLACE FUNCTION zs_upd_real_estates()
  RETURNS trigger AS
$BODY$
  BEGIN

    IF (TG_OP = 'INSERT' OR
        (OLD.props::text IS DISTINCT FROM NEW.props::text) OR
        (OLD."geoResult"::text IS DISTINCT FROM NEW."geoResult"::text)) THEN
            NEW = zs_calculate_columns(NEW);
            NEW.intersected = zs_calculate_intersected_single(NEW."geoResult", NEW.online);
    END IF;

    NEW.modified = NOW();
    RETURN NEW;
  END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION zs_upd_real_estates()
  OWNER TO zoomsquare;
