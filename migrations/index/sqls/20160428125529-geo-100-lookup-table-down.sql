/* Replace with your SQL commands */
DROP SERVER if exists bdm2 cascade;
drop TABLE loc_entity_geo;
drop function bdm_simplify_to_n_points(geo geometry, npoints integer);
drop function zs_populate_loc_entity_geo();
drop function bdm_prepareGeoJSON(geometry);
drop function zs_populate_loc_entity_geo_from_fgn();