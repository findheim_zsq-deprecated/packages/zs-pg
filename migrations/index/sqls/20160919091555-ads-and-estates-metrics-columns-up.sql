/* Replace with your SQL commands */

create table real_estate_ads_metrics (
 "extId" text PRIMARY KEY references real_estate_ads("extId"),
  favtrash jsonb,
  favtrash_ts timestamp with time zone NOT NULL
);

CREATE OR REPLACE FUNCTION zs_upsert_real_estate_ads_metrics(
    extId_val text,
    favtrash_val jsonb,
    favtrash_ts_val timestamp with time zone
) RETURNS void AS
$BODY$
BEGIN
  LOOP
    UPDATE
      real_estate_ads_metrics
    SET
      favtrash = favtrash_val,
      favtrash_ts = favtrash_ts_val
    WHERE
      "extId" = extId_val;
    IF found THEN
      EXIT;
    END IF;
    BEGIN
      INSERT INTO real_estate_ads_metrics(
        "extId",
        favtrash,
        favtrash_ts
      ) VALUES (
        extId_val,
        favtrash_val,
        favtrash_ts_val
      );
    EXIT;
    EXCEPTION WHEN unique_violation THEN
      -- LOOP AGAIN!
    END;
  END LOOP;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE;

CREATE OR REPLACE FUNCTION bdm_preparegeojson(geo geometry, prec int)
  RETURNS geometry AS
$BODY$
BEGIN
    -- we got alot of invalid repeated points after conversion to geoJSON
    -- we try to fix this here
    RETURN ST_makeValid(ST_RemoveRepeatedPoints(ST_geomFromGeoJSON(ST_asGeoJSON(geo,prec))));
END;
$BODY$
  LANGUAGE plpgsql IMMUTABLE STRICT
  COST 100;
ALTER FUNCTION bdm_preparegeojson(geometry, int)
  OWNER TO zoomsquare;