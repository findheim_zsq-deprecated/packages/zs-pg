--not doing anything here, index handling moved to a tool
--
---- indexed identifiers
--DROP INDEX IF EXISTS real_estates_fqdn_idx;
--DROP INDEX IF EXISTS real_estates_dups_idx;
--DROP INDEX IF EXISTS real_estates_meta_idx;
--DROP INDEX IF EXISTS real_estates_extId_idx;
--
---- indexes for important json props
--DROP INDEX IF EXISTS real_estates_props_ownType_idx;
--DROP INDEX IF EXISTS real_estates_props_useType_idx;
--DROP INDEX IF EXISTS real_estates_props_investmentOnly_idx;
--DROP INDEX IF EXISTS real_estates_props_cooperative_idx;
--DROP INDEX IF EXISTS real_estates_props_councilHome_idx;
--DROP INDEX IF EXISTS real_estates_props_noCommission_idx;
--DROP INDEX IF EXISTS real_estates_geo_precision_idx;
--DROP INDEX IF EXISTS real_estates_geo_type_idx;
--DROP INDEX IF EXISTS real_estates_geo_id_idx;
--
---- indexed columns
--DROP INDEX IF EXISTS real_estates_livingSpace_idx;
--DROP INDEX IF EXISTS real_estates_plotArea_idx;
--DROP INDEX IF EXISTS real_estates_usableSpace_idx;
--DROP INDEX IF EXISTS real_estates_roomSpace_idx;
--DROP INDEX IF EXISTS real_estates_rentalFee_idx;
--DROP INDEX IF EXISTS real_estates_purchasePrice_idx;
--DROP INDEX IF EXISTS real_estates_annualLeaseFee_idx;
--DROP INDEX IF EXISTS real_estates_rooms_idx;
--DROP INDEX IF EXISTS real_estates_type_idx;
--DROP INDEX IF EXISTS real_estates_geo_idx;
--DROP INDEX IF EXISTS real_estates_array_length_dups;
--
---- indexed derived fields
--DROP INDEX IF EXISTS real_estates_intersected_idx;
--
---- indexed meta data
--DROP INDEX IF EXISTS real_estates_online_idx;
--DROP INDEX IF EXISTS real_estates_created_idx;
--DROP INDEX IF EXISTS real_estates_modified_idx;
--DROP INDEX IF EXISTS real_estates_updated_idx;
--DROP INDEX IF EXISTS real_estates_unixtime_modified_idx;
--DROP INDEX IF EXISTS real_estates_bigImagesLength_idx;
--DROP INDEX IF EXISTS real_estates_props__ver_idx;