/* Replace with your SQL commands */
drop function if exists zs_timeonmarket(real_estates);
drop function if exists zs_timeonmarket(real_estate_ads);
drop function if exists zs_timeonmarket(text);
drop index IF EXISTS "immohosts_fqdn_idx";
drop INDEX IF EXISTS "immohosts_blacklist_days_idx";
DROP INDEX IF EXISTS "immohosts_blacklist_days_created_idx";
DROP INDEX IF EXISTS "immohosts_created_idx";
