CREATE OR REPLACE FUNCTION zs_array_difference(
    array1 text[],
    array2 text[])
  RETURNS text[] AS
$BODY$
declare
    res text[];
begin
    if array1 is null then
        return null;
    elseif array2 is null then
        return array1;
    end if;
    
    select array_agg(e) into res
    from (
        select unnest(array1)
        except
        select unnest(array2)
    ) as dt(e);
    return res;
end;
$BODY$
  LANGUAGE plpgsql IMMUTABLE
  COST 100;