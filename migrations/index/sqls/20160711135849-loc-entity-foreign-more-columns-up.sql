alter foreign table loc_entity add column hidden boolean;
alter foreign table loc_entity add column city boolean;
alter foreign table loc_entity add column name text;
alter foreign table loc_entity add column area double precision;