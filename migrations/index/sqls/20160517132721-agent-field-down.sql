
ALTER TABLE real_estate_ads drop COLUMN person;
ALTER TABLE real_estate_ads drop COLUMN "premiumAd";
ALTER TABLE real_estates drop COLUMN persons;
ALTER TABLE real_estates drop COLUMN dups_persons;

drop function zs_upsert_real_estates(
    url_val text,
    extid_val text,
    fqdn_val text,
    dups_val text[],
    props_val jsonb,
    georesult_val jsonb,
    sources_val json,
    thumb_val text,
    bigimages_val text[],
    online_val boolean,
    created_val timestamp with time zone,
    updated_val timestamp with time zone,
    dups_fqdns_val text[],
    dups_urls_val text[],
    dups_online_val boolean[],
    stats_val jsonb,
    seo_snippets_val text[],
    oi_val jsonb,
    text[],
    jsonb,
    timestamp with time zone);