/* Replace with your SQL commands */
DROP TRIGGER zs_estate_ads_constraint_trigger ON real_estates;
DROP FUNCTION IF EXISTS zs_estate_ads_constraint();
ALTER table real_estate_ads ALTER "real_estates_extId" DROP NOT NULL;