-- Function: zs_upd_real_estates()

drop function if exists zs_calculate_intersected_single(georesult json,online boolean);

CREATE OR REPLACE FUNCTION zs_upd_real_estates()
  RETURNS trigger AS
$BODY$
  BEGIN

    IF (TG_OP = 'INSERT' OR (OLD.props::text IS DISTINCT FROM NEW.props::text)) THEN
       NEW = zs_calculate_columns(NEW);
    END IF;

    IF ((TG_OP = 'INSERT') OR
      (OLD."geoResult"->>'intersected') <> (NEW."geoResult"->>'intersected') OR (OLD."geoResult"->>'fake') = 'true') or new.intersected is null THEN

      new.intersected = zs_jsonb_arr2text_arr((NEW."geoResult"->'intersected')::jsonb);

    END IF;

    NEW.modified = NOW();
    RETURN NEW;
  END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION zs_upd_real_estates()
  OWNER TO zoomsquare;
