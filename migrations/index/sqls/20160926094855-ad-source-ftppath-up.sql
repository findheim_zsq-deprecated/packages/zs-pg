/* Replace with your SQL commands */

alter table ad_sources add column ftppath text;

CREATE OR REPLACE FUNCTION zs_upsert_ad_sources(
    extId_val   text,
    content_val text,
    source_val  text,
url_val text,
fqdn_val text,
ftppath_val text) RETURNS void AS
$BODY$
BEGIN
  LOOP
    UPDATE
      ad_sources
    SET
      "content" = content_val,
      "source" = source_val,
      url = url_val,
      updated = now(),
      fqdn = fqdn_val,
      ftppath =ftppath_val
    WHERE
      "extId" = extId_val;
    IF found THEN
      EXIT;
    END IF;
    BEGIN
      INSERT INTO ad_sources(
        "extId",
        source,
        content,
        url,
        fqdn,
        ftppath
      ) VALUES (
        extId_val,
        source_val,
        content_val,
        url_val,
        fqdn_val,
        ftppath_val
      );
    EXIT;
    EXCEPTION WHEN unique_violation THEN
      -- LOOP AGAIN!
    END;
  END LOOP;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE;