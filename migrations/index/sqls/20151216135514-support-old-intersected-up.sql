-- Function: zs_upd_real_estates()
CREATE EXTENSION IF NOT EXISTS plcoffee;

-- DROP FUNCTION zs_upd_real_estates();
CREATE OR REPLACE FUNCTION zs_calculate_intersected_single(georesult jsonb)
  RETURNS text[] AS
$BODY$
unless georesult?
  result = []
else if georesult?.intersected
  result = georesult.intersected
else
  result = []
  for item in georesult.topo or []
    result.push item.id
  if georesult.type <= 10
    result.push georesult.id

return result

$BODY$
  LANGUAGE plcoffee IMMUTABLE
  COST 100;


CREATE OR REPLACE FUNCTION zs_upd_real_estates()
  RETURNS trigger AS
$BODY$
  BEGIN

    IF (TG_OP = 'INSERT' OR (OLD.props::text IS DISTINCT FROM NEW.props::text)) THEN
       NEW = zs_calculate_columns(NEW);
    END IF;

    IF ((TG_OP = 'INSERT') OR
      (OLD."geoResult"->>'intersected') <> (NEW."geoResult"->>'intersected')) or OLD.intersected is null THEN

      new.intersected = zs_calculate_intersected_single(NEW."geoResult");

    END IF;

    NEW.modified = NOW();
    RETURN NEW;
  END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

/* --------- --------- --------- FUNCTION --------- --------- --------- --------- */
--DROP FUNCTION zs_topo3_deleteme(r real_estates);

CREATE OR REPLACE FUNCTION zs_georesult_get_from_ads(r real_estates)
  RETURNS jsonb AS
$$

  console=
    log: (args...) ->
      plv8.elog NOTICE, args

  max = (arr) ->
    arr.sort()[arr.length-1]

  affected = plv8.execute """select geo from real_estate_ads where "real_estates_extId" = $1""", [r.extId]
  result = null
  for item in affected
    if result?
      if item.geo._ver is "3.0.1" or result.type < item.geo.type
        result = item.geo
    else
      result = item.geo

  return result
$$
  LANGUAGE plcoffee volatile
  COST 100;
