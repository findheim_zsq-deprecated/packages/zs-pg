/* Replace with your SQL commands */

ALTER TABLE real_estates ALTER COLUMN created DROP NOT NULL;
ALTER TABLE real_estates DROP CONSTRAINT estates_created_not_too_old;
ALTER TABLE real_estates ALTER COLUMN online DROP NOT NULL;


ALTER TABLE real_estate_ads ALTER COLUMN online DROP NOT NULL;
ALTER TABLE real_estate_ads ALTER COLUMN created DROP NOT NULL;
ALTER TABLE real_estate_ads ALTER COLUMN "lastCrawled" DROP NOT NULL;
ALTER TABLE real_estate_ads ALTER COLUMN "lastWrittenToPG" DROP NOT NULL;
ALTER TABLE real_estate_ads ALTER COLUMN "lastPipelined" DROP NOT NULL;
ALTER TABLE real_estate_ads DROP CONSTRAINT ads_created_not_too_old;
ALTER TABLE real_estate_ads DROP CONSTRAINT last_offline_not_null;
