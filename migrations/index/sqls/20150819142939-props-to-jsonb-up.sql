/* Replace with your SQL commands */
ALTER TABLE real_estates
  ALTER COLUMN props
  SET DATA TYPE jsonb
  USING props::jsonb;

CREATE OR REPLACE FUNCTION zs_upsert_newpipe(
    url_val text,
    extid_val text,
    fqdn_val text,
    dups_val text[],
    props_val jsonb,
    georesult_val json,
    sources_val json,
    thumb_val text,
    bigimages_val text[],
    online_val boolean,
    created_val timestamp with time zone,
    updated_val timestamp with time zone,
    dups_fqdns_val text[],
    dups_urls_val text[],
    dups_online_val boolean[],
    stats_val jsonb)
  RETURNS void AS
$BODY$
BEGIN
  LOOP
    UPDATE
      real_estates
    SET
      url = url_val,
      "extId" = extId_val,
      fqdn = fqdn_val,
      dups = dups_val,

      props = props_val,
      "geoResult" = geoResult_val,
      sources = sources_val,

      thumb = thumb_val,
      "bigImages" = bigImages_val,

      online = online_val,
      created = created_val,
      updated = updated_val,

      dups_fqdns = dups_fqdns_val,
      dups_urls = dups_urls_val,
      dups_online = dups_online_val,
      stats = stats_val
    WHERE
      "extId" = extId_val;
    IF found THEN
      EXIT;
    END IF;
    BEGIN
      INSERT INTO
        real_estates(
          url,
          "extId",
          fqdn,
          dups,

          props,
          "geoResult",
          sources,

          thumb,
          "bigImages",

          online,
          created,
          updated,

          dups_fqdns,
          dups_urls,
          dups_online,
          stats
        ) VALUES (
          url_val,
          extId_val,
          fqdn_val,
          dups_val,

          props_val,
          geoResult_val,
          sources_val,

          thumb_val,
          bigImages_val,

          online_val,
          created_val,
          updated_val,

          dups_fqdns_val,
          dups_urls_val,
          dups_online_val,
          stats_val
        );
      EXIT;
    EXCEPTION WHEN unique_violation THEN
      -- LOOP AGAIN!
    END;
  END LOOP;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION zs_upsert_newpipe(text, text, text, text[], jsonb, json, json, text, text[], boolean, timestamp with time zone, timestamp with time zone, text[], text[], boolean[], jsonb)
  OWNER TO zoomsquare;

CREATE OR REPLACE FUNCTION zs_calculate_columns(r real_estates)
  RETURNS real_estates AS
$BODY$
  BEGIN
    r."livingSpace" = cast(r.props->>'livingSpace' as numeric);
    r."plotArea" = cast(r.props->>'plotArea' as numeric);
    r."usableSpace" = cast(r.props->>'usableSpace' as numeric);
    r."roomSpace" = cast(r.props->>'roomSpace' as numeric);

    r."rentalFee" = cast(r.props->>'rentalFee' as numeric);
    r."purchasePrice" = cast(r.props->>'purchasePrice' as numeric);
    r."annualLeaseFee" = cast(r.props->>'annualLeaseFee' as numeric);

    r.rooms = cast(r.props->>'rooms' as numeric);
    if (r."props"->>'type') IS NULL or (r."props"->>'type') = '' or (r."props"->>'type') = '[]' then
        r.type = ARRAY[]::text[]; -- fix for saveToPostgresNewFormat: Could not push to Postgres: cannot call json_array_elements on a scalar: (type was null)
    else
        r.type = (SELECT array_agg(trim(el::text, '"')) FROM jsonb_array_elements(r."props"->'type') el)::text[];
    END IF;

    IF (r."geoResult"->>'precision') != 'georef' THEN
        r.geo = (SELECT ST_SetSRID(ST_GeomFromGeoJSON(r."geoResult"->>'geo'), 4326));
    END IF;

    RETURN r;
  END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;