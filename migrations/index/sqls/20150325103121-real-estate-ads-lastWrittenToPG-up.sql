/* Replace with your SQL commands */
ALTER TABLE real_estate_ads
    ADD COLUMN "lastWrittenToPG" timestamp with time zone;

CREATE OR REPLACE FUNCTION zs_real_estate_ads_update_or_insert()
  RETURNS trigger AS
$BODY$
  BEGIN

    NEW."lastWrittenToPG"= NOW();
    RETURN NEW;
  END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

CREATE TRIGGER zs_real_estate_ads_update_or_insert_trigger
  BEFORE INSERT OR UPDATE
  ON real_estate_ads
  FOR EACH ROW
  EXECUTE PROCEDURE zs_real_estate_ads_update_or_insert();
