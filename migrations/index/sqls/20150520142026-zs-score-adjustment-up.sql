/* Replace with your SQL commands */

-- SELECT cast(zs_score_v1(70::numeric, 500::numeric, 3::numeric, 12::numeric, COALESCE(0::numeric, NULL::numeric, NULL::numeric), COALESCE( 1000::numeric, NULL::numeric, NULL::numeric ), ARRAY[]::text[], ARRAY[]::integer[], (extract(epoch from now())::integer - zs_unixtime(p.created)), p ) as numeric(6,3)) from real_estates p

CREATE OR REPLACE FUNCTION zs_score_v1(
    minspace numeric,
    maxspace numeric,
    minrooms numeric,
    maxrooms numeric,
    minprice numeric,
    maxprice numeric,
    features text[],
    featureimportances integer[],
    stats_names text[],
    stats_importances integer[],
    timedifference integer,
    p real_estates)
  RETURNS numeric AS
$BODY$
  import math
  import json
  scores = []

  if minspace:
      minspace_float = float(minspace)
  else:
      minspace_float = None

  if maxspace:
      maxspace_float = float(maxspace)
  else:
      maxspace_float = None

  if minrooms:
      minrooms_float = float(minrooms)
  else:
      minrooms_float = None

  if maxrooms:
      maxrooms_float = float(maxrooms)
  else:
      maxrooms_float = None

  if minprice:
      minprice_float = float(minprice)
  else:
      minprice_float = None

  if maxprice:
      maxprice_float = float(maxprice)
  else:
      maxprice_float = None


  ### Scoring functions ###
  # calculates f(val) where f is a band function defined by:
  #     val ranges: hardmin <= min          <=  max         <= hardmax
  #     f(val)      0          hardminval       hardmaxval     0
  # params:
  #     ramp_diff: desired slope between min and max
  #     strength:
  def band_custom_ramp(val, hardmin, hardminval, min, max, hardmax, hardmaxval, strength, ramp_diff, weight):

      if min is None and max is None:
          return

      if min is None:
          min = 0.0
      if max is None:
          max = 2.0**32
      if hardmin is None:
          hardmin = min
      if hardmax is None:
          hardmax = max

      if val is None:
          res = 0.0
      elif min <= val <= max: # inside user entered bounds
          if max == min:
              res = 1.0
          else:
              if ramp_diff < 0:
                  tomax = 1.0*(val - min)/(max-min)
              else:
                  tomax = 1.0*(max - val)/(max-min)
              res = 1.0 - tomax*math.fabs(ramp_diff)
      elif val <= hardmin or val >= hardmax: # outside hardmax bounds
          res = 0.0
      else: # outside user entered bounds, inside hardmax bounds
          if val > max:
              tohard = 1.0 * (val - max) / (hardmax - max)
              hardval = hardmaxval
          else:
              tohard = 1.0 * (val - min) / (hardmin - min)
              hardval = hardminval

          res = 1.0 * hardval / (1 + strength * math.exp(tohard * 12 - 6))

      scores.append([res, weight])

  def ageScore(differenceInSeconds, weight):
      if differenceInSeconds - 60 * 60 * 24 * 7 > 0:
          scores.append([0.0001, weight])
      else:
          ageScore = 1 - (differenceInSeconds / (60 * 60 * 24 * 7 * 1.0))
          scores.append([ageScore, weight])

  ageScore(timedifference, 12.0)

  props = json.loads(p["props"])

  for costType in ["rentalFee", "purchasePrice", "annualLeaseFee"]:
      if costType in props and props[costType]:
          band_custom_ramp(props[costType], 0.5 * minprice_float, 0.95, minprice_float, maxprice_float, 1.1 * maxprice_float, 0.9, 1.0, -0.1, 17.0)
          break

  if minrooms_float is not None and maxrooms_float is not None:
      band_custom_ramp(props["rooms"], minrooms_float, 1.0, minrooms_float, maxrooms_float, 2.0 * maxrooms_float, 0.95, 1.0, 0.1, 15.0)
  if minspace_float is not None and maxspace_float is not None:
      band_custom_ramp(props["livingSpace"], 0.85 * minspace_float, 0.9, minspace_float, maxspace_float, 2.0 * maxspace_float, 0.95, 1.0, 0.1,15.0)

  # We don't have plotarea, but asked for it, so punish the Estate
  # if p.get("plotarea") == None and plotarea != None:
  #   penalize(2.0)

  # penalize big sites w/o large images, TODO: remove this hard coded hack
  #  if re.search('(immobilien\.net|immoversum\.com|finden\.at|derstandard\.at)', p.get('url', '')):
  #    return 1

  if p.get("bigImages") is None or len(p.get("bigImages")) is 0:
      scores.append([0.0,10.0])
  else:
      scores.append([1.0,10.0])

  # FEATURES
  # filter out boolean props (features) and save their key names
  estate_features = dict(filter(lambda (key,val): type(val) is bool, props.iteritems())).keys()
  if not estate_features:
      estate_features = []

  # fmax is the maximum attainable importance sum
  fmax = 0.0

  for imp in featureimportances:
      fmax += imp

  if fmax: # we have wanted features
      fscore = 0.0

      for i, wantedfeature in enumerate(features):
          if estate_features.count(wantedfeature):  # estate has wanted feature
              fscore += featureimportances[i]

      scores.append([fscore / fmax, 18.0])
  else: # no wanted features: just add number of estate features
      scores.append([min(len(estate_features),20.0)/20.0, 18 ])


  # STATS (greenscore,..)
  if len(stats_importances): # user wants stats
      fmax = 0.0
      for imp in stats_importances:
          fmax += imp*100.0
      if p.get("stats") is None:
          scores.append([0 / fmax, 18.0])
      else:
          estate_stats = json.loads(p["stats"])
          fscore = 0.0

          for i, wanted_stats_name in enumerate(stats_names):
              if estate_stats.keys().count(wanted_stats_name):  # estate has wanted stat
                  fscore += stats_importances[i] * float(estate_stats.get(wanted_stats_name))

          scores.append([fscore / fmax, 18.0])

  # Weightening
  dividend = 0.0
  divisor = 0.0
  for score in scores:
      dividend = dividend + (score[0] * score[1])
      divisor = divisor + score[1]

  if divisor == 0:
      return 0.0
  else:
      return dividend / divisor * 100.0


$BODY$
  LANGUAGE plpython2u VOLATILE
  COST 100;
ALTER FUNCTION zs_score_v1(numeric, numeric, numeric, numeric, numeric, numeric, text[], integer[], text[], integer[], integer, real_estates)
  OWNER TO zoomsquare;
