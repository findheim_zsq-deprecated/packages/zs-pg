DROP TRIGGER IF EXISTS zs_push_trigger ON real_estates;
DROP FUNCTION IF EXISTS zs_reverse_query();
DROP FUNCTION IF EXISTS zs_upsert_searchprofile(numeric[],numeric[],numeric[],numeric[],numeric[],text[],text,text,text[],integer[],boolean,boolean);
DROP FUNCTION IF EXISTS zs_push_notify(text, text);
