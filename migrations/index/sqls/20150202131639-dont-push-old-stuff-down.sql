/* Replace with your SQL commands */

-- Function: zs_reverse_query()

-- DROP FUNCTION zs_reverse_query();

CREATE OR REPLACE FUNCTION zs_reverse_query()
  RETURNS trigger AS
$BODY$
BEGIN
  IF array_length(new.dups, 1) = 1 THEN
    PERFORM
      zs_push_notify(row_to_json(row)::text, row.id::text)
    FROM (
      SELECT
        sp.id,
        new."extId",
        new.type,
        new."geoResult"->>'label' as address,
        new.props->>'livingSpace' as "livingSpace",
        new.created as created,
        COALESCE(new.props->>'rentalFee', new.props->>'purchasePrice', new.props->>'annualLeaseFee') as price,
        (
          CASE
            WHEN (new.props->>'rentalFee') IS NOT NULL THEN 'rent'
            WHEN (new.props->>'purchasePrice') IS NOT NULL THEN 'purchase'
            WHEN (new.props->>'annualLeaseFee') IS NOT NULL THEN 'lease'
          END
        ) as "ownType"
      FROM searchprofiles sp
      WHERE
        (sp."lastPush" IS NULL OR sp."lastPush" < (now() - interval '50 minutes')) -- push only every 3 hours
        AND sp.enabled -- push only to push enabled
        AND sp."geoEntities" && new.intersected -- geo must match
        AND sp.type = ANY(new.type) -- type must match
        AND ( --
          CASE
            WHEN (sp."rentalFee")      IS NOT NULL THEN (new.props->>'rentalFee')::numeric      BETWEEN sp."rentalFee"[1] AND sp."rentalFee"[2]
            WHEN (sp."purchasePrice")  IS NOT NULL THEN (new.props->>'purchasePrice')::numeric  BETWEEN sp."purchasePrice"[1] AND sp."purchasePrice"[2]
            WHEN (sp."annualLeaseFee") IS NOT NULL THEN (new.props->>'annualLeaseFee')::numeric BETWEEN sp."annualLeaseFee"[1] AND sp."annualLeaseFee"[2]
          END
        )
        AND (
          CASE
            WHEN (sp."livingSpace") IS NOT NULL THEN
              (new.props->>'livingSpace') IS NOT NULL AND (
                ((new.props->>'livingSpace')::numeric BETWEEN sp."livingSpace"[1] AND sp."livingSpace"[2]) OR
                (sp."livingSpace"[1] IS NULL AND sp."livingSpace"[2] >= (new.props->>'livingSpace')::numeric) OR
                (sp."livingSpace"[2] IS NULL AND sp."livingSpace"[1] <= (new.props->>'livingSpace')::numeric)
              )
            ELSE
              TRUE
          END
        )
        AND (
          CASE
            WHEN (sp.rooms) IS NOT NULL THEN
              (new.props->>'rooms') IS NOT NULL AND (
                ((new.props->>'rooms')::numeric BETWEEN sp.rooms[1] AND sp.rooms[2]) OR
                (rooms[1] IS NULL AND rooms[2] >= (new.props->>'rooms')::numeric) OR
                (rooms[2] IS NULL AND rooms[1] <= (new.props->>'rooms')::numeric)
              )
          ELSE
            TRUE
          END
        )
        AND (
          CASE WHEN sp."noCommission"::boolean THEN
            (new.props->>'noCommission') IS NOT NULL AND (new.props->>'noCommission')::boolean
          ELSE
            TRUE
          END
        )
      ) row;
  END IF;
  RETURN new;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION zs_reverse_query()
  OWNER TO zoomsquare;


-- Function: zs_newproperty_notify()

-- DROP FUNCTION zs_newproperty_notify();

CREATE OR REPLACE FUNCTION zs_newproperty_notify()
  RETURNS trigger AS
$BODY$BEGIN
    IF array_length(NEW.dups, 1) > 0 THEN
        PERFORM pg_notify('newproperty', row_to_json(row)::text) FROM (
            SELECT
            -- similar format than zs_reverse_query
            -- but no searchprofile id
            -- +url
            -- ownType instead of reasoning via prices... => and domain is "rented, purchased, leased" NOT "rent", etc.!
            -- +viewType
            -- +all spaces
            -- +lat, lng for map display
                NEW."extId",
                NEW.url,
                NEW.type,
                NEW.props->>'viewType' AS "viewType",
                NEW.props->>'ownType' AS "ownType",
                NEW."geoResult"->>'label' AS address,
                NEW.props->>'livingSpace' AS "livingSpace",
                NEW.props->>'plotArea' AS "plotArea",
                NEW.props->>'roomSpace' AS "roomSpace",
                NEW.props->>'usableSpace' AS "usableSpace",
                COALESCE(NEW.props->>'rentalFee', NEW.props->>'purchasePrice', NEW.props->>'annualLeaseFee') AS price,
                NEW.props->>'rooms' AS rooms,
                NEW.props->>'noCommission' AS "noCommission",
                NEW.props->>'investmentOnly' AS "investmentOnly",
                NEW.props->>'cooperative' AS cooperative,
                NEW.props->>'openSpace' AS "openSpace",
                NEW.props->>'balcony' AS balcony,
                NEW.props->>'terrace' AS terrace,
                NEW.props->>'roofTerrace' AS "roofTerrace",
                NEW.props->>'pool' AS pool,
                NEW.props->>'wintergarden' AS wintergarden,
                NEW.props->>'roofTop' AS "roofTop",
                NEW.props->>'nearbyUnderground' AS "nearbyUnderground",
                NEW.props->>'needsRenovation' AS "needsRenovation",
                NEW.props->>'barrierFree' AS "barrierFree",
                NEW.props->>'wheelChairSuitable' AS "wheelChairSuitable",
                NEW.props->>'newBuilding' AS "newBuilding",
                NEW.props->>'oldBuilding' AS "oldBuilding",
                NEW."geoResult"->>'lat' AS lat,
                NEW."geoResult"->>'lng' AS lng,
                NEW."geoResult"->>'precision' AS precision,
                NEW.intersected,
                NEW.props->'units' AS units,
                NEW.thumb,
                NEW.created
            ) row;
    END IF;
    RETURN new;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION zs_newproperty_notify()
  OWNER TO zoomsquare;
