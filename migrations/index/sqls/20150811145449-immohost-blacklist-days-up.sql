CREATE TABLE immohosts
(
  fqdn text NOT NULL,
  created timestamp with time zone DEFAULT now(),
  blacklist_days date[],
  blacklist_days_created timestamp with time zone,
  CONSTRAINT "fqdn" PRIMARY KEY ("fqdn")
);