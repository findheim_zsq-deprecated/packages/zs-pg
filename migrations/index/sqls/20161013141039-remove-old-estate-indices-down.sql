/* Replace with your SQL commands */

CREATE INDEX "real_estates_props_noCommission_null_idx"
  ON real_estates
  USING btree
  (((props ->> 'noCommission'::text) IS NULL));

CREATE INDEX "real_estates_props_noCommission_idx"
  ON real_estates
  USING btree
  ((props ->> 'noCommission'::text) COLLATE pg_catalog."default");

CREATE INDEX real_estates_props__ver_idx
  ON real_estates
  USING btree
  ((props ->> '_ver'::text) COLLATE pg_catalog."default");

CREATE INDEX real_estates_created_idx
  ON real_estates
  USING btree
  (zs_unixtime(created));

CREATE INDEX "real_estates_bigImagesNull_idx"
  ON real_estates
  USING btree
  ((array_length("bigImages", 1) IS NULL));

CREATE INDEX "real_estates_annualLeaseFee_idx"
  ON real_estates
  USING btree
  ("annualLeaseFee");