DROP TRIGGER zs_upd_real_estates_trigger ON real_estates;
DROP TRIGGER zs_newproperty_trigger ON real_estates;
DROP FUNCTION IF EXISTS zs_newproperty_notify();
DROP FUNCTION IF EXISTS zs_upd_real_estates();
DROP FUNCTION IF EXISTS zs_calculate_intersected(real_estates);
DROP FUNCTION IF EXISTS zs_calculate_columns(real_estates);
