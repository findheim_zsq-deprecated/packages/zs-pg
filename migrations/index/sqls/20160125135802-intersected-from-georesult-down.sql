/* Replace with your SQL commands */

-- Function: zs_upd_real_estates()

-- DROP FUNCTION zs_upd_real_estates();

CREATE OR REPLACE FUNCTION zs_upd_real_estates()
  RETURNS trigger AS
$BODY$
  BEGIN

    IF (TG_OP = 'INSERT' OR (OLD.props::text IS DISTINCT FROM NEW.props::text)) THEN
       NEW = zs_calculate_columns(NEW);
    END IF;

    IF ((TG_OP = 'INSERT') OR
      (OLD."geoResult"->>'intersected') <> (NEW."geoResult"->>'intersected')) or OLD.intersected is null THEN

      new.intersected = zs_calculate_intersected_single(NEW."geoResult");

    END IF;

    NEW.modified = NOW();
    RETURN NEW;
  END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION zs_upd_real_estates()
  OWNER TO zoomsquare;


