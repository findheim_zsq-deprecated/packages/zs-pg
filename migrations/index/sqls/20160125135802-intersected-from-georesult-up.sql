-- Function: zs_upd_real_estates()

-- DROP FUNCTION zs_upd_real_estates();

CREATE OR REPLACE FUNCTION zs_upd_real_estates()
  RETURNS trigger AS
$BODY$
  BEGIN

    IF (TG_OP = 'INSERT' OR (OLD.props::text IS DISTINCT FROM NEW.props::text)) THEN
       NEW = zs_calculate_columns(NEW);
    END IF;

    IF    (TG_OP = 'INSERT')
       OR (OLD."geoResult"->>'intersected') IS DISTINCT FROM (NEW."geoResult"->>'intersected')
       OR array_length(OLD.intersected,1) is null THEN

      new.intersected = zs_jsonb_arr2text_arr(NEW."geoResult"->'intersected');

    END IF;

    NEW.modified = NOW();
    RETURN NEW;
  END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION zs_upd_real_estates()
  OWNER TO zoomsquare;
