CREATE OR REPLACE FUNCTION zs_upd_real_estates()
  RETURNS trigger AS
$BODY$
  BEGIN

    IF (TG_OP = 'INSERT' or (OLD.props::text IS DISTINCT FROM NEW.props::text)) THEN
        NEW = zs_calculate_columns(NEW);
    END IF;

    IF (TG_OP = 'INSERT' or (OLD."geoResult"::text IS DISTINCT FROM NEW."geoResult"::text)) THEN
        NEW = zs_calculate_intersected(NEW);
    END IF;

    NEW.modified = NOW();
    RETURN NEW;
  END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION zs_upd_real_estates()
  OWNER TO zoomsquare;

DROP FUNCTION IF EXISTS zs_calculate_intersected_single(json,boolean);
