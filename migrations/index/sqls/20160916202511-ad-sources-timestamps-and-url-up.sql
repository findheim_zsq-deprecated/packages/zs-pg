/* Replace with your SQL commands */

alter table ad_sources add column created timestamp with time zone DEFAULT now();
alter table ad_sources add column updated timestamp with time zone;
alter table ad_sources add column url text;
alter table ad_sources add column fqdn text;

CREATE OR REPLACE FUNCTION zs_upsert_ad_sources(
    extId_val   text,
    content_val text,
    source_val  text,
url_val text,
fqdn_val text) RETURNS void AS
$BODY$
BEGIN
  LOOP
    UPDATE
      ad_sources
    SET
      "content" = content_val,
      "source" = source_val,
      url = url_val,
      updated = now(),
      fqdn = fqdn_val
    WHERE
      "extId" = extId_val;
    IF found THEN
      EXIT;
    END IF;
    BEGIN
      INSERT INTO ad_sources(
        "extId",
        source,
        content,
        url,
        fqdn
      ) VALUES (
        extId_val,
        source_val,
        content_val,
        url_val,
        fqdn_val
      );
    EXIT;
    EXCEPTION WHEN unique_violation THEN
      -- LOOP AGAIN!
    END;
  END LOOP;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE;