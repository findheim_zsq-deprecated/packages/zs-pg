DROP TABLE real_estates CASCADE;
DROP FUNCTION zs_unixtime(timestamp with time zone);
-- don't drop so we can keep BDM topology intact when re-running migrations... DROP SCHEMA postgis CASCADE;
-- don't drop plpython2u since score functions could be installed still: DROP EXTENSION plpython2u;