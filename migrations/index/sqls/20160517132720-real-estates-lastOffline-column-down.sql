alter table real_estates drop column "lastOffline";

DROP FUNCTION zs_upsert_newpipe3(
    url_val text,
    extid_val text,
    fqdn_val text,
    dups_val text[],
    props_val jsonb,
    georesult_val jsonb,
    sources_val json,
    thumb_val text,
    bigimages_val text[],
    online_val boolean,
    created_val timestamp with time zone,
    updated_val timestamp with time zone,
    dups_fqdns_val text[],
    dups_urls_val text[],
    dups_online_val boolean[],
    stats_val jsonb,
    seo_snippets_val text[],
    lastoffline_val timestamp with time zone);
