/* Replace with your SQL commands */

drop table real_estate_ads_metrics;
drop function if exists zs_upsert_real_estate_ads_metrics(
    extId_val text,
    favtrash_val jsonb,
    favtrash_ts_val timestamp with time zone
);
