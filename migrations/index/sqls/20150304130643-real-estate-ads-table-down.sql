/* Replace with your SQL commands */

ALTER TABLE real_estates
    ADD COLUMN id text;

CREATE INDEX "real_estates_id_idx" ON real_estates USING hash (id);

DROP TABLE real_estate_ads;
