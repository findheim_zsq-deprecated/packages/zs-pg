/* Replace with your SQL commands */

DROP TRIGGER zs_real_estate_ads_update_or_insert_trigger ON real_estate_ads;
DROP FUNCTION IF EXISTS zs_real_estate_ads_update_or_insert();
ALTER TABLE real_estate_ads DROP COLUMN IF EXISTS "lastWrittenToPG";