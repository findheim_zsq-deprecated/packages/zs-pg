alter table real_estates
  drop column seo_snippets;

DROP FUNCTION if exists zs_upsert_newpipe2(text, text, text, text[], jsonb, jsonb, json, text, text[], boolean, timestamp with time zone, timestamp with time zone, text[], text[], boolean[], jsonb);