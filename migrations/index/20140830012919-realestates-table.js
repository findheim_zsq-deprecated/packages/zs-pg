var dbm = require('db-migrate');
var type = dbm.dataType;
var fs = require('fs');
var path = require('path');

exports.up = function(db, callback) {
    var filePath = path.join(__dirname + '/sqls/20140830012919-realestates-table-up.sql');
    fs.readFile(filePath, {encoding: 'utf-8'}, function(err,data){
        if (err) return console.log(err);
        console.log('received data: ' + data);

        db.runSql(data, function(err) {
            if (err) return console.log(err);
            callback();
        });
    });
};

exports.down = function(db, callback) {
    var filePath = path.join(__dirname + '/sqls/20140830012919-realestates-table-down.sql');
    fs.readFile(filePath, {encoding: 'utf-8'}, function(err,data){
        if (err) return console.log(err);
        console.log('received data: ' + data);

        db.runSql(data, function(err) {
            if (err) return console.log(err);
            callback();
        });
    });
};
