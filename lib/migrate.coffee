fs = require 'fs'

# CAUTION: during migrations we DO NOT import 'zs-app/lib/config' - it might confuse db-migrate with other pg defaults!
throw new Error("use db-migrate directly!")

# returns up/down migrations for db-migrate and does a PRODUCTION env safety check
module.exports = (migration) ->
  if process.env.ZS_ENV is 'production' and not env.get('ZS_FORCE_MIGRATION', null)
    throw new Error "Migration not allowed to run in PRODUCTION environment, please set environment variable ZS_FORCE_MIGRATION if you are really sure!"

  return {
    up: (db, callback) ->
      db.runSql fs.readFileSync(process.cwd() + "/migrations/#{migration}/up.sql", 'utf8'), callback
    down: (db, callback) ->
      db.runSql fs.readFileSync(process.cwd() + "/migrations/#{migration}/down.sql", 'utf8'), callback
  }