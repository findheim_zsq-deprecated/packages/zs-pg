async = require "async"
_     = require "lodash"
Transaction = require('pg-transaction')
util  = require "../util"

removeId = (arr) ->
  _.map arr, (itm) ->
    delete itm._id
    itm

module.exports = class Immohost
  @fromMongo: (mongoImmohost, client) ->
    #console.log JSON.stringify mongoImmohost, false, 4
    try
      pgImmohost =
        fqdn:                   mongoImmohost.fqdn
        created:                new Date(parseInt(mongoImmohost._id.toString().substring(0,8),16)*1000)
        sitemap_urls:           mongoImmohost.sitemaps
        openimmo_urls:          mongoImmohost.openimmo
        crawl_delay:            mongoImmohost.crawlDelay

        state:                  mongoImmohost.state
        comment:                mongoImmohost.comment
        issues:                 mongoImmohost.issues
        estimated_ad_count:     mongoImmohost.numEstates
        last_estate_found:      mongoImmohost.lastEstateFound

        last_offline_found:     mongoImmohost.lastOfflineFound
        last_zoomlive_push:     mongoImmohost.lastZoomLivePush
        crawler_id:             mongoImmohost.crawlerId
        testpipeline:           mongoImmohost.testpipeline or false
        prioritize_new_estates: mongoImmohost.prioritizeNewEstates or false

        justimmo:               mongoImmohost.config.justimmo or false
        push_disabled:          mongoImmohost.config.pushDisabled or false
        prefer_https:           mongoImmohost.config.preferHTTPS or false
        check_selectors:        mongoImmohost.config.check
        image_selector:         mongoImmohost.config.image?.jpath

        image_attr:             mongoImmohost.config.image?.attr
        id_regex:               mongoImmohost.config.regex?.source
        follow_regex:           mongoImmohost.config.follow?.source
        title_selector:         mongoImmohost.config.title?.jpath
        fragments:              removeId mongoImmohost.config.fragments

        seeds:                  removeId mongoImmohost.config.seeds
        zionic:                 mongoImmohost.config.zionic or {}
        rights_big_images:      mongoImmohost.rights?.bigImages or false
        rights_gallery:         mongoImmohost.rights?.gallery or false
    catch e
      console.log e
      console.log mongoImmohost
      throw e

    console.log JSON.stringify pgImmohost, false, 4

    new Immohost client, pgImmohost, removeId mongoImmohost._changes

  @import: (MongoImmoHost, client, cb) ->
    client.query "truncate immohost cascade;", (err, res) ->
      if err
        return cb err
      console.log res
      MongoImmoHost.find().lean().exec (err, immohosts) ->
        async.eachSeries immohosts, (immohost, cb) ->
          host = Immohost.fromMongo immohost, client
          console.log "inserting #{host.data.fqdn}"
          host.insert cb
        , cb

  constructor: (@client, @data, @changes = []) ->
    unless @data.fqdn?
      throw new Error "fqdn missing"

  insert: (cb) ->
    self = @
    tx = new Transaction @client
    sql = """
insert into immohost (
  fqdn,                
  created,
  sitemap_urls,
  openimmo_urls,
  crawl_delay,

  state,
  comment,
  issues,
  estimated_ad_count,
  last_estate_found,

  last_offline_found,
  last_zoomlive_push,
  crawler_id,
  testpipeline,
  prioritize_new_estates,

  justimmo,
  push_disabled,
  prefer_https,

  check_selectors,
  image_selector,
  image_attr,
  id_regex,
  follow_regex,

  title_selector,
  fragments,
  seeds,
  zionic,
  rights_big_images,

  rights_gallery
) VALUES (
  $1,
  $2,
  $3,
  $4,
  $5,
  $6,
  $7,
  $8,
  $9,
  $10,
  $11,
  $12,
  $13,
  $14,
  $15,
  $16,
  $17,
  $18,
  $19,
  $20,
  $21,
  $22,
  $23,
  $24,
  $25::jsonb,
  $26::jsonb,
  $27::jsonb,
  $28,
  $29
  );
"""
    params = [
      @data.fqdn
      @data.created
      @data.sitemap_urls
      @data.openimmo_urls
      @data.crawl_delay

      @data.state
      @data.comment
      @data.issues
      @data.estimated_ad_count
      @data.last_estate_found

      @data.last_offline_found
      @data.last_zoomlive_push
      @data.crawler_id
      @data.testpipeline
      @data.prioritize_new_estates

      @data.justimmo
      @data.push_disabled
      @data.prefer_https
      @data.check_selectors
      @data.image_selector

      @data.image_attr
      @data.id_regex
      @data.follow_regex
      @data.title_selector
      JSON.stringify @data.fragments

      JSON.stringify @data.seeds
      JSON.stringify @data.zionic
      @data.rights_big_images
      @data.rights_gallery
    ]

    tx.query sql, params, (err, res) ->
      if err
        return cb err
      else if res.rowCount isnt 1
        return cb new Error "rowCount was #{res.rowCount}"

      async.each self.changes, (change, cb) ->
        tx.query """insert into immohost_changes (fqdn, time, diff, comment) values ($1,$2,$3::jsonb,$4)""", [
          self.data.fqdn
          change.time
          JSON.stringify change.diff
          change.comment
        ], cb
      , (err) ->
        if err
          return tx.abort ->
            cb err

        tx.commit cb