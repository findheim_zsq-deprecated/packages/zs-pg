class Queries
  searchable: """ FROM real_estates WHERE 
    online
    AND type && ARRAY['Apartment', 'House', 'Plot']
    AND intersected && ARRAY['10400000000001']
    AND ("livingSpace" > 0 OR type && ARRAY['Plot'])
    AND ("rentalFee" > 0 OR "purchasePrice"> 0)
    AND ((props->>'investmentOnly') is null)
    AND ((props->>'councilHome') is null)
    AND ((props->>'cooperative') is null) """
  
module.exports = new Queries()
  