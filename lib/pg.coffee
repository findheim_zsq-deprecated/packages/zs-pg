try
  statics = require("parent-require")("zs-statics")
catch
  statics = require "zs-statics"

pg = require 'pg'
delete pg.native # delete the stupid getter: https://github.com/sequelize/sequelize/issues/3781
_ = require "lodash"
async = require "async"

Transaction = require 'pg-transaction'
#try
#  if pg.native?
#    pg = pg.native
#    console.error "zs-pg: using *native* bindings"
#  else
#    console.error "zs-pg: using *js* bindings"
#catch
#  console.error "zs-pg: using *js* bindings"

# lets not use native. selfddos happened again (5.3.2015)

pools = {}

process.on "exit", ->
  for key, pool of pools
    pool.end()

module.exports =
  defaultLane: 'test'

  queries: require './queries'

  util: require "./util"

  model: require "./model"

  txCreate: (pg_client) ->
    new Transaction(pg_client)

  setPoolSize: (num) ->
    pg.defaults.poolSize = num
  getPoolSize: ->
    pg.defaults.poolSize

  getLib: ->
    pg

  setDefaultLane: (laneOrConnstr)->
    console.error "ZS-PG: setting default lane to '#{laneOrConnstr}'"
    @defaultLane = laneOrConnstr

  _getConnStr: (lane) ->
    unless lane?
      lane = @defaultLane
    unless lane in Object.keys(statics.pg)
      lane
    else
      statics.getDBConfig("pg", lane)

  getClient: (lane) ->
    new pg.Client @_getConnStr(lane)

  getClientFromPool: (cb, lane) ->
    pools[lane] ?= new pg.Pool @_getConnStr(lane)
    pools[lane].connect cb

  queryPool: (sql, params, cb, lane) ->
    if not cb? or _.isString cb # 2 args: sql, cb[, lane]
      if _.isFunction params
        lane = cb
        cb = params
        params = []
      else
        throw new Error "cb missing"
    else if not _.isArray params # 3 args: sql, params, cb
      cb new Error "params not an array"
    else if not _.isFunction cb
      cb new Error "cb not a function"

    unless _.isString sql
      cb new Error "sql must be String"

    @getClientFromPool (err, client, clientDone) ->
      if err?
        clientDone()
        cb new Error "Error getting client from pool: #{err.message}"
      else
        client.query sql, params, (err, res) ->
          clientDone()
          if err?
            cb err
          else
            cb null, res
    , lane


# usage: https://github.com/brianc/node-pg-cursor
#
# require("zs-pg").queryBatch "select asdf", [4], 100, (err, rows, next) ->
#   if err
#     throw err
#   else
#     processRows(rows)
#     next()
# , "de.index"
  queryBatch: (sql, params, batchSize, cb, lane) ->
    unless _.isFunction cb
      if _.isFunction batchSize # 3 args: sql, params, cb
        cb = batchSize
        batchSize = 100
      else if _.isFunction params # 2 args: sql, cb
        cb = params
        params = []
        batchSize = 100
      else
        throw new Error "cb missing"
    unless _.isString sql
      setImmediate ->
        cb new Error "Parameter sql must be a string"
    unless _.isArray params
      setImmediate ->
        cb new Error "Parameter params must be an array"
    unless _.isFinite batchSize
      setImmediate ->
        cb new Error "Parameter batchSize must be a finite number"

    Cursor = require('pg-cursor')

    @getClientFromPool (err, client) ->
      if err
        cb err
      else
        cur = client.query new Cursor(sql, params)
        read = ->
          cur.read batchSize, (err, rows) ->
            if err
              cb err
            else
              cb null, rows, () ->
                read()
        read()
    , lane
  # usage:
  # require("zs-pg").queryStream "SELECT asdf limit $1", [10], "at.index", (err, stream) ->
  #   if err?
  #     throw err
  #   stream.pipe(JSONStream.stringify()).pipe(process.stdout)
  queryStream: (sql, params = [], lane, cb)->
    QueryStream = require "pg-query-stream"
    @getClientFromPool (err, client, done) ->
      if err
        return cb err
      stream = client.query new QueryStream sql, params
      stream.on 'end', done
      cb null, stream
    , lane
  # usage:
  # require("zs-pg").queryStreamToJSONFile
  #   sql: "SELECT x from y where z = $1"
  #   params: ["foo"]
  #   lane: <lane>
  #   report: int, report every x rows
  #   filename: relative to cwd
  #   streamCB: (stream) ->
  #     stream.on "data", (row) ->
  #       console.log row
  # , (err) ->
  #   if err
  #     console.error err
  #   else
  #     console.log "done"
  queryStreamToJSONFile: (options = {}, doneCB)->

    JSONStream = require "JSONStream"
    fs = require "fs"

    start = new Date
    rows = 0
    @queryStream options.sql, options.params or [], options.lane, (err, stream) ->
      if err?
        return doneCB err

      ws = fs.createWriteStream options.filename

      stream.on "end", ->
        if options.report
          console.log "dumpToJSONFile DONE: #{rows} estates, #{Math.round(1000 * rows / (new Date() - start))} per second"
        setImmediate ->
          doneCB null

      stream.on "data", ->
        rows++
        if options.report && rows %% options.report is 0
          console.log "dumpToJSONFile PROGRESS: #{rows} results, #{Math.round(1000 * rows / (new Date() - start))} per second"

      stream.pipe(JSONStream.stringify()).pipe(ws)
      if options.streamCB
        options.streamCB stream

if process.env.ZS_PG_LANE?
  console.error "ZS-PG: found ZS_PG_LANE=#{process.env.ZS_PG_LANE}."
  module.exports.setDefaultLane process.env.ZS_PG_LANE
else if process.env.ZS_ENV is "production"
  console.error "ZS-PG: found ZS_ENV=#{process.env.ZS_ENV}."
  module.exports.setDefaultLane "at.index"
else if process.env.NODE_ENV is "production"
  console.error "ZS-PG: found NODE_ENV=#{process.env.NODE_ENV}."
  module.exports.setDefaultLane "at.index"
else
  console.error "ZS-PG: no relevant env set, setting default lane to 'search2test'"
  module.exports.setDefaultLane "search2test"