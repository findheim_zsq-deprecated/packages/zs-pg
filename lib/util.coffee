_ = require 'lodash'
escape = require "pg-escape"

module.exports =
  escape: escape
  fillQuery: (qry, queryParams) ->
    queryParams = _.cloneDeep queryParams
    for param, index in queryParams
      if _.isArray param
        if param.length > 0
          params = param.map (p) ->
            "'#{p}'"
          param = "ARRAY[#{params.join(", ")}]"
        else
          param = "ARRAY[]"
      else if _.isString param
        param = "'" + param.replace(/'/g, "\\'") + "'"
      else if _.isBoolean param
        param = if param then 'TRUE' else 'FALSE'
      else if not param?
        param = 'NULL'
      qry = qry.replace new RegExp("\\$#{index + 1}(?!\\d)", "g"), param
    qry = qry.replace /--.*/g, "" # remove comments
    qry

  createUpdateQuery: (row, table, pk = "id") ->
    unless table?
      throw new Error "table missing"

    sql = """
    update
      #{table}
    set

    """
    params = []
    updates = []
    for key, value of row
      if key is pk
        continue
      params.push value
      updates.push "  \"#{key}\"=$#{params.length}"

    sql += updates.join ",\n"

    params.push row[pk]
    sql += """

    where
      "#{pk}" = $#{params.length}
    """
    {sql, params} #, sqlFilled: @fillQuery(sql, params)}

