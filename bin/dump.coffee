#!/usr/bin/env coffee
fs = require "fs"

commander = require "commander"

pg = require "../"
statics = require "zs-statics"

commander
.option "-p, --pglane <lane>", "pg lane (default: ZS_PG_LANE)"
.option "-o, --output <filename>", "output filename (default: dump.json)", "dump.json"
.option "-f, --format <name>", "format (only json for now)", "json"
.option "-r, --pretty", "prettyprint output (indent json) (default: false)", false
.option "-i, --input <file>", "sql input file name (default: null)", null

commander
.command "query <sql>"
.description "dump result of any sql. sql string might need quoting with '' or \"\" or some combination thereof."
.action (sql) ->
  console.log """
dumping
  #{sql}
from
  #{commander.pglane} (#{statics.pg[commander.pglane]?.host})
to
  #{commander.output}
in format
  #{commander.format}
"""
  pg.queryPool sql, (err, res) ->
    if err
      console.error err.message
    else
      if commander.pretty
        str = JSON.stringify res.rows, false, 4
      else
        str = JSON.stringify res.rows
      require("fs").writeFileSync commander.output, str
      console.log "done."
      process.exit 0
  , commander.pglane

commander
.command "convert <file>"
.description ""
.action (file) ->
  console.log file
  x = require "../#{file}.json"
  res = {}
  for y in x
    if res[y.id_v2]?
      throw new Error "id collision"
    unless  y.id_v3?
      console.log "adding empty mapping"
    res[y.id_v2] = y.id_v3

  console.log "#{process.cwd()}/#{file}.conv.json"
  fs.writeFileSync "#{process.cwd()}/#{file}.conv.json", JSON.stringify(res)

commander.parse process.argv